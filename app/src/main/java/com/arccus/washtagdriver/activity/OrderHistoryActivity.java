package com.arccus.washtagdriver.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.OrderHistoryAdapter;
import com.arccus.washtagdriver.model.OrderHistoryData;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.arccus.washtagdriver.utils.Helper.CUSTOMER_ID;

public class OrderHistoryActivity extends AppCompatActivity {

    private RecyclerView rvOrderHistory;
    private LinearLayoutManager orderhistoryLayoutmanager;
    private OrderHistoryAdapter orderHistoryAdapter;
    private OrderHistoryData orderHistoryData;
    private ArrayList<OrderHistoryData> orderHistoryArrayList;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvOrderHistory = (RecyclerView) findViewById(R.id.rvOrderHistory);
        orderhistoryLayoutmanager = new LinearLayoutManager(OrderHistoryActivity.this);
        orderhistoryLayoutmanager.scrollToPosition(0);
        // overlayLayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvOrderHistory.setLayoutManager(orderhistoryLayoutmanager);
        rvOrderHistory.setHasFixedSize(true);


        getOrderHistory();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
    }

    private void getOrderHistory(){

        String customer_id = CUSTOMER_ID;

        if (Helper.isNetworkAvailable(OrderHistoryActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("ORDER LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            orderHistoryArrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String order_no = result.getString("order_no");
                                String order_date = result.getString("order_date");


                                orderHistoryData = new OrderHistoryData();
                                orderHistoryData.setOrder_no(order_no);
                                orderHistoryData.setOrder_date(order_date);

                                orderHistoryArrayList.add(orderHistoryData);
                            }

                            orderHistoryAdapter = new OrderHistoryAdapter( OrderHistoryActivity.this, orderHistoryArrayList);

                            rvOrderHistory.setAdapter(orderHistoryAdapter);

                            int resId = R.anim.layout_animation_from_bottom;
                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(OrderHistoryActivity.this, resId);
                            rvOrderHistory.setLayoutAnimation(animation);

                        } else {
                            Helper.showToast(OrderHistoryActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(OrderHistoryActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", customer_id);

            API api = new API(OrderHistoryActivity.this, apiResponse);
            api.execute(1, Services.GET_ORDER, params, true);

        } else {
            Helper.showToast(OrderHistoryActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
