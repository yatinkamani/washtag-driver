package com.arccus.washtagdriver.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.SelectCustomerAdapter;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.model.CustomerDataList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.arccus.washtagdriver.utils.Helper.CUSTOMER_ID;

public class SelectCustomerActivity extends AppCompatActivity {

  //  String[] language ={"C","C++","Java",".NET","iPhone","Android","ASP.NET","Azroo","PHP"};
  //  private LinearLayout llNext;
   // private  AutoCompleteTextView actv;
    private RecyclerView rvCustomerList;
    private LinearLayoutManager customerLayoutmanager;
    private SelectCustomerAdapter selectCustomerAdapter;
    private CustomerDataList customerDataList;
    private ArrayList<CustomerDataList> customerArrayList;
    private ImageView ivSearch, ivBack;

    private RelativeLayout rlMain;
    EditText editTextSearch;
    private String driver_id;
    ArrayList<String> names;

    private ClothDataList clothDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        SharedPreferences sp = SelectCustomerActivity.this.getSharedPreferences("Driver", Context.MODE_PRIVATE);
        driver_id = sp.getString("driver_id", "").trim();

        Helper.CUSTOMER_NAME = "";
        CUSTOMER_ID = "";

     //   llNext = (LinearLayout) findViewById(R.id.llNext);
        rlMain = (RelativeLayout) findViewById(R.id.rlMain);
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);

        ivSearch = (ImageView) findViewById(R.id.ivSearch);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvCustomerList = (RecyclerView) findViewById(R.id.rvCustomerList);
        customerLayoutmanager = new LinearLayoutManager(SelectCustomerActivity.this);
        customerLayoutmanager.scrollToPosition(0);
        // overlayLayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCustomerList.setLayoutManager(customerLayoutmanager);
        rvCustomerList.setHasFixedSize(true);

        getCustomerList();

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this,android.R.layout.select_dialog_item,language);
        //Getting the instance of AutoCompleteTextView
        actv =  (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        actv.setThreshold(1);//will start working from first character
        actv.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
*/

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.requestFocus();
                String text = editTextSearch.getText().toString().trim();
                if(TextUtils.isEmpty(text)){
                    Helper.showToast(SelectCustomerActivity.this, "Please enter name");
                } else {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rlMain.getWindowToken(), 0);
                }
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<String> filterdNames = new ArrayList<>();

        if(names.size()>0) {
            //looping through existing elements
            for (String s : names) {
                //if the existing elements contains the search input
                if (s.toLowerCase().contains(text.toLowerCase())) {
                    //adding the element to filtered list
                    filterdNames.add(s);
                }
            }

        //calling a method of the adapter class and passing the filtered list
        selectCustomerAdapter.filterList(filterdNames);
        }
    }

    private void getCustomerList(){

        names = new ArrayList<>();

        if (Helper.isNetworkAvailable(SelectCustomerActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("CUSTOMER LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            customerArrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                              //  String book_id = result.getString("book_id");
                                String user_id = result.getString("user_id");
                                String user_name = result.getString("user_name");
                                String customer_no = result.getString("customer_no");

                                customerDataList = new CustomerDataList();
                             //   customerDataList.setBook_id(book_id);
                                customerDataList.setCustomer_id(user_id);
                                customerDataList.setCustomer_name(customer_no + "." + user_name);

                                customerArrayList.add(customerDataList);
                                names.add(customer_no + "." + user_name);
                            }

                            selectCustomerAdapter = new SelectCustomerAdapter( SelectCustomerActivity.this, names, customerArrayList, 1);
                            rvCustomerList.setAdapter(selectCustomerAdapter);

                            int resId = R.anim.layout_animation_from_bottom;
                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(SelectCustomerActivity.this, resId);
                            rvCustomerList.setLayoutAnimation(animation);

                        } else {
                            Helper.showToast(SelectCustomerActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(SelectCustomerActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", driver_id);

            API api = new API(SelectCustomerActivity.this, apiResponse);
            api.execute(1, Services.GET_ACTIVE_CUSTOMER_LIST, params, true);

        } else {
            Helper.showToast(SelectCustomerActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
