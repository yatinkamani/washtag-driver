package com.arccus.washtagdriver.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.SelectIronUserAdapter;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.model.CustomerDataList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.arccus.washtagdriver.utils.Helper.IRON_USER_ID;

public class SelectIronUserActivity extends AppCompatActivity {

    //  String[] language ={"C","C++","Java",".NET","iPhone","Android","ASP.NET","Ao","PHP"};
    //  private LinearLayout llNext;
    // private  AutoCompleteTextView active;
    private RecyclerView rvCustomerList;
    private LinearLayoutManager customerLayoutManager;
    private SelectIronUserAdapter selectCustomerAdapter;
    private CustomerDataList customerDataList;
    private ArrayList<CustomerDataList> customerArrayList;
    private ImageView ivSearch, ivBack;
    TextView txt_title;

    private RelativeLayout rlMain;
    EditText editTextSearch;
    private String driver_id;
    ArrayList<String> names;

    private ClothDataList ClothDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        txt_title = findViewById(R.id.txt_title);
        txt_title.setText(Helper.INDIVIDUAL_SERVICE_NAME + " User List");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        SharedPreferences sp = SelectIronUserActivity.this.getSharedPreferences("Driver", Context.MODE_PRIVATE);
        driver_id = sp.getString("driver_id", "").trim();

        Helper.CUSTOMER_NAME = "";
        IRON_USER_ID = "";

        //   llNext = (LinearLayout) findViewById(R.id.llNext);
        rlMain = (RelativeLayout) findViewById(R.id.rlMain);
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);

        ivSearch = (ImageView) findViewById(R.id.ivSearch);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvCustomerList = (RecyclerView) findViewById(R.id.rvCustomerList);
        customerLayoutManager = new LinearLayoutManager(SelectIronUserActivity.this);
        customerLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCustomerList.setLayoutManager(customerLayoutManager);
        rvCustomerList.setHasFixedSize(true);

        getCustomerList();

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this,android.R.layout.select_dialog_item,language);
        //Getting the instance of AutoCompleteTextView
        active =  (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        active.setThreshold(1);//will start working from first character
        active.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView/*/

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.requestFocus();
                String text = editTextSearch.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    Helper.showToast(SelectIronUserActivity.this, "Please enter name");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rlMain.getWindowToken(), 0);
                }
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<String> filterNames = new ArrayList<>();

        if (names.size() > 0) {
            //looping through existing elements
            for (String s : names) {
                //if the existing elements contains the search input
                if (s.toLowerCase().contains(text.toLowerCase())) {
                    //adding the element to filtered list
                    filterNames.add(s);
                }
            }

            //calling a method of the adapter class and passing the filtered list
            selectCustomerAdapter.filterList(filterNames);
        }
    }

    private void getCustomerList() {

        names = new ArrayList<>();

        if (Helper.isNetworkAvailable(SelectIronUserActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    response = Html.fromHtml(response).toString();
                    response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                    Helper.showLog("CUSTOMER LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            customerArrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                //  String book_id = result.getString("book_id");
                                String user_id = result.getString("iron_user_id");
                                String user_name = result.getString("name");
                                String customer_no = result.getString("customer_id");
                                String service_type = result.getString("service_type");

                                customerDataList = new CustomerDataList();
                                //  customerDataList.setBook_id(book_id);
                                customerDataList.setCustomer_id(user_id);
                                customerDataList.setCustomer_name(customer_no + "." + user_name);

                                if (service_type.equals("") || service_type.equals(Helper.INDIVIDUAL_SERVICE_NAME)){
                                    customerArrayList.add(customerDataList);
                                    names.add(customer_no + "." + user_name);
                                }
                            }

                            selectCustomerAdapter = new SelectIronUserAdapter(SelectIronUserActivity.this, names, customerArrayList, 1);
                            rvCustomerList.setAdapter(selectCustomerAdapter);

                            int resId = R.anim.layout_animation_from_bottom;
                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(SelectIronUserActivity.this, resId);
                            rvCustomerList.setLayoutAnimation(animation);

                        } else {
                            Helper.showToast(SelectIronUserActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(SelectIronUserActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", driver_id);

            API api = new API(SelectIronUserActivity.this, apiResponse);
            api.execute(1, Services.GET_ACTIVE_IRON_CUSTOMER_LIST, params, true);

        } else {
            Helper.showToast(SelectIronUserActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

}
