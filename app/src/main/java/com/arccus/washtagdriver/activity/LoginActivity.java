package com.arccus.washtagdriver.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import static com.arccus.washtagdriver.utils.Validate.isValidMobile;

public class LoginActivity extends AppCompatActivity {

    private LinearLayout llRegister, llforgot_password, rootView;
    private EditText etEmail, etPassword;
    private boolean isPermission = false;
    private DatabaseHelper mDatabase;
    private ClothDataList clothDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //requestPermission();

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);

        mDatabase = new DatabaseHelper(LoginActivity.this);
    }

    public void onLogIn(View view){
       // if(isPermission) {
            String email = etEmail.getText().toString().trim();
            String password = etPassword.getText().toString().trim();

            if (TextUtils.isEmpty(email)) {
                Helper.showToast(LoginActivity.this, "Enter mobile no");
            } else if (!isValidMobile(email)) {
                Helper.showToast(LoginActivity.this, "Enter valid mobile no");
            } else if (TextUtils.isEmpty(password)) {
                Helper.showToast(LoginActivity.this, "Enter password");
            } else {
                if (Helper.isNetworkAvailable(LoginActivity.this)) {

                 //   getClothDetails();

                    APIResponse apiResponse = new APIResponse() {
                        @Override
                        public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                            Helper.showLog("LOGIN: " + response);
                            try {
                                JSONObject object = new JSONObject(response);
                                String status = object.getString("status");
                                String message = object.getString("message");
                                if (status.equalsIgnoreCase("true")) {

                                    JSONArray data = object.getJSONArray("result");

                                    for (int i = 0; i < data.length(); i++) {

                                        JSONObject result = data.getJSONObject(i);
                                        String driver_id = result.getString("driver_id");
                                        String driver_name = result.getString("driver_name");
                                        String address = result.getString("address");
                                        String phone = result.getString("phone");
                                        String email = result.getString("email");

                                        SharedPreferences sp = getApplicationContext().getSharedPreferences("Driver", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sp.edit();
                                        editor.putString("driver_id", driver_id);
                                        editor.putString("driver_name", driver_name);
                                        editor.putString("address", address);
                                        editor.putString("phone_no", phone);
                                        editor.putString("email", email);
                                        editor.apply();
                                    }

                                    getClothDetails();

                                } else {
                                    Helper.showToast(LoginActivity.this, message);
                                }
                            } catch (JSONException e) {
                                Helper.showLog("LOGIN Catch: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onAPIError(int requestCode, boolean isError, String error) {

                            if (isError) {
                                Helper.showLog("LOGIN Error: " + error);
                                Helper.showToast(LoginActivity.this, error);
                            }
                        }
                    };

                    HashMap<String, String> params = new HashMap<>();
                    params.put("phone", email);
                    params.put("password", password);

                    API api = new API(LoginActivity.this, apiResponse);
                    api.execute(1, Services.LOGIN, params, true);

                }else {
                    Helper.showToast(LoginActivity.this, Services.NO_NETWORK);
                }
            }
        /*} else {
            requestPermission();
        }*/
    }

    private void getClothDetails(){

        if (Helper.isNetworkAvailable(LoginActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("GET ALL CLOTH : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            Log.e("TOTAL CLOTH NUMBER:",""+data.length());
                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String garment_id = result.getString("garment_id");
                                String garment_category_id = result.getString("garment_category_id");
                                String garment_category_name = result.getString("garment_category_name");
                                String garment_name = result.getString("garment_name");
                                String garment_image = result.getString("garment_image");

                                clothDataList = new ClothDataList();
                                clothDataList.setCloth_id(garment_id);
                                clothDataList.setClothCategory(garment_category_id);
                                clothDataList.setClothName(garment_name);
                                clothDataList.setNumber_of_Cloth("0");
                                clothDataList.setClothUri(garment_image);
                                clothDataList.setClothBitmap(null);

                                SaveIntoDatabase task = new SaveIntoDatabase();
                                task.execute(clothDataList);
                            }
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        } else {
                            Helper.showToast(LoginActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(LoginActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(LoginActivity.this, apiResponse);
            api.execute(1, Services.GET_CLOTH, params, true);
        } else {
            Helper.showToast(LoginActivity.this, Services.NO_NETWORK);
        }
    }

    public class SaveIntoDatabase extends AsyncTask<ClothDataList,Void,Void> {
        // can use UI thread here
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        // automatically done on worker thread (separate from UI thread)
        @Override
        protected Void doInBackground(ClothDataList... params) {
            ClothDataList dataModel = params[0];
            ClothDataList clothDataList1 = new ClothDataList();
            try {
                String image_url = dataModel.getClothUri();
                InputStream inputStream = new URL(image_url).openStream();
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                clothDataList1.setCloth_id(dataModel.getCloth_id());
                clothDataList1.setClothName(dataModel.getClothName());
                clothDataList1.setNumber_of_Cloth("0");
                clothDataList1.setClothCategory(dataModel.getClothCategory());
                clothDataList1.setClothBitmap(bitmap);
                //add data to database (shows in next step)
                mDatabase.addData(clothDataList1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void requestPermission(){

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                            isPermission = true;
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}
