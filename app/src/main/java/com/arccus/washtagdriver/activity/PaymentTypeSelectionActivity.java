package com.arccus.washtagdriver.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.arccus.washtagdriver.R;

public class PaymentTypeSelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_type_selection);
    }
}
