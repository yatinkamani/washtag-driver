package com.arccus.washtagdriver.activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arccus.washtagdriver.R;

public class OtpVerificationActivity extends AppCompatActivity {

    private EditText inputOtp;
    private Button btn_verify_otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        /*inputOtp = (EditText) findViewById(R.id.inputOtp);
        btn_verify_otp = (Button) findViewById(R.id.btn_verify_otp);

        SmsReceiver.bindListener(new SmsListner() {
            @Override
            public void messageReceived(String messageText) {
                Log.e("OTP","TRUE");
                inputOtp.setText(messageText);
            }
        });

        btn_verify_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verifyOTP();
            }
        });*/
    }


    public void verifyOTP(){
        String otp = inputOtp.getText().toString().trim();
        if (!otp.isEmpty()) {
            Intent intent = new Intent(OtpVerificationActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }else {
            Toast.makeText(OtpVerificationActivity.this, "Please enter the OTP", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();

        Intent intent = new Intent(OtpVerificationActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
