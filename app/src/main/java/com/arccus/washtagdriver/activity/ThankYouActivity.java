package com.arccus.washtagdriver.activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arccus.washtagdriver.R;

import static com.arccus.washtagdriver.utils.Helper.CUSTOMER_NAME;

public class ThankYouActivity extends AppCompatActivity {

    private LinearLayout llNext;
    private TextView tvCustomerName;

    private String customer_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);

        llNext = (LinearLayout) findViewById(R.id.llNext);
        tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);

        customer_name = CUSTOMER_NAME;

        tvCustomerName.setText(customer_name);

        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();

        Intent intent = new Intent(ThankYouActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
