package com.arccus.washtagdriver.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.OrderedClothAdapter;
import com.arccus.washtagdriver.model.OrderedClothList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class IronOrderDetailsActivity extends AppCompatActivity {

    private ImageView ivBack;
    private RecyclerView rvClothList;
    private LinearLayoutManager orderedClothLayoutManager;
    private OrderedClothAdapter orderedClothAdapter;
    private OrderedClothList orderedClothList;
    private ArrayList<OrderedClothList> orderedClothArrayList;
    private TextView tv_customer_name, tv_order_no, tv_order_date, tv_customer_address,
            tv_delivery_date, tv_service_type, tv_total_cloth, tv_order_status, tv_deliver;

    private String orderNo = "";
    private String total_cloth;
    private boolean IsChanges = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        orderNo = getIntent().getStringExtra("ORDER_NO");

        ivBack = (ImageView) findViewById(R.id.ivBack);

        tv_customer_name = (TextView) findViewById(R.id.tv_customer_name);
        tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        tv_order_date = (TextView) findViewById(R.id.tv_order_date);
        tv_customer_address = (TextView) findViewById(R.id.tv_customer_address);
        tv_delivery_date = (TextView) findViewById(R.id.tv_delivery_date);
        tv_service_type = (TextView) findViewById(R.id.tv_service_type);
        tv_total_cloth = (TextView) findViewById(R.id.tv_total_cloth);
        tv_order_status = (TextView) findViewById(R.id.tv_order_status);
        tv_deliver = (TextView) findViewById(R.id.tv_deliver);

        rvClothList = (RecyclerView) findViewById(R.id.rvClothList);
        orderedClothLayoutManager = new LinearLayoutManager(IronOrderDetailsActivity.this);
        orderedClothLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvClothList.setLayoutManager(orderedClothLayoutManager);
        rvClothList.setHasFixedSize(true);

        if (!TextUtils.isEmpty(orderNo)) {
            getOrderDetail();
        }

        tv_deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(IronOrderDetailsActivity.this);
                alertDialogBuilder.setMessage("Are you sure you want to deliver this order?");
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                setDelivery();
                            }
                        });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getOrderDetail() {

        if (Helper.isNetworkAvailable(IronOrderDetailsActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("IRON ORDER DETAILS : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            orderedClothArrayList = new ArrayList<>();

                            JSONObject result = object.getJSONObject("result");

                            String user_name = result.getString("name");
                            String order_no = result.getString("order_no");
                            String order_date = result.getString("order_date");
                            String address = result.getString("address");
                            String delivery_date = result.getString("delivery_date");
                            total_cloth = result.getString("total_clothes");
                            String order_status = result.getString("order_status");
                            String service_type = result.has("service_type") ? result.getString("service_type") : "";

                            tv_customer_name.setText(user_name);
                            tv_order_no.setText(order_no);
                            tv_order_date.setText(order_date);
                            tv_customer_address.setText(address);
                            tv_delivery_date.setText("Delivery Date: " + delivery_date);
                            tv_service_type.setText("Types of Service: " + Helper.INDIVIDUAL_SERVICE_NAME);
                            tv_total_cloth.setText(total_cloth);

                            if (order_status.equalsIgnoreCase("1")) {
                                tv_order_status.setText("Delivered");
                                tv_delivery_date.setVisibility(View.VISIBLE);
                                tv_deliver.setVisibility(View.GONE);
                            } else {
                                tv_delivery_date.setVisibility(View.GONE);
                                tv_order_status.setText("Pending");
                                tv_deliver.setVisibility(View.VISIBLE);
                            }

                            JSONArray type_of_service = result.getJSONArray("type_of_service");

                            for (int i = 0; i < type_of_service.length(); i++) {

                                JSONObject clDetail = type_of_service.getJSONObject(i);

                                String no_of_garments = clDetail.getString("no_of_garments");
                                String garment_name = clDetail.getString("garment_name");

                                orderedClothList = new OrderedClothList();
                                orderedClothList.setCloth_name(garment_name);
                                orderedClothList.setCloth_quantity(no_of_garments);

                                orderedClothArrayList.add(orderedClothList);
                            }

                            orderedClothAdapter = new OrderedClothAdapter(IronOrderDetailsActivity.this, orderedClothArrayList);

                            rvClothList.setAdapter(orderedClothAdapter);

                            int resId = R.anim.layout_animation_from_bottom;
                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(IronOrderDetailsActivity.this, resId);
                            rvClothList.setLayoutAnimation(animation);

                        } else {
                            Helper.showToast(IronOrderDetailsActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(IronOrderDetailsActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("order_no", orderNo);

            API api = new API(IronOrderDetailsActivity.this, apiResponse);
            api.execute(1, Services.GET_ORDER_DETAIL_IRON, params, true);

        } else {
            Helper.showToast(IronOrderDetailsActivity.this, Services.NO_NETWORK);
        }
    }

    private void setDelivery() {

        if (Helper.isNetworkAvailable(IronOrderDetailsActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("IRON ORDER DELIVERY : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            /* JSONObject result = object.getJSONObject("data");

                            String delivery_date = result.getString("delivery_date");

                            tv_delivery_date.setVisibility(View.VISIBLE);
                            tv_delivery_date.setText("Delivery Date: " + delivery_date);
                            tv_order_status.setText("Delivered");

                            tv_deliver.setVisibility(View.GONE);*/

                            Helper.showToast(IronOrderDetailsActivity.this, message);
                            Intent in = new Intent();
                            setResult(RESULT_OK, in);
                            finish();

                        } else {
                            Helper.showToast(IronOrderDetailsActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(IronOrderDetailsActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("order_no", orderNo);
            params.put("total_clothes", total_cloth);

            API api = new API(IronOrderDetailsActivity.this, apiResponse);
            api.execute(1, Services.SET_DELIVERY_IRON, params, true);

        } else {
            Helper.showToast(IronOrderDetailsActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (IsChanges) {
            Intent in = new Intent();
            setResult(RESULT_OK, in);
        }
        finish();
    }
}
