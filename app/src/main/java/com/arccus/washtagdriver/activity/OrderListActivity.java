package com.arccus.washtagdriver.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.OrderListAdapter;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.model.OrderDataList;
import com.arccus.washtagdriver.model.SelectedOrderList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;
import com.simplecityapps.recyclerview_fastscroll.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.arccus.washtagdriver.utils.Helper.IRON_USER_ID;
import static com.arccus.washtagdriver.utils.Helper.TOTAL_CLOTH_COUNT;

public class OrderListActivity extends AppCompatActivity {

    private RecyclerView rvOrderList;
    private LinearLayoutManager orderListLayoutManager;
    private OrderListAdapter orderListAdapter;
    public OrderDataList orderDataList;
    private ArrayList<OrderDataList> orderDataArrayList;

    private DatabaseHelper mDatabase;
    private LinearLayout llPlaceOrder;
    private String driver_id;
    TextView tvDate;
    String orderDateTime = "";
    DatePickerDialog datePickerDialog;

    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mDatabase = new DatabaseHelper(OrderListActivity.this);

        Intent intent = this.getIntent();
        ArrayList<SelectedOrderList> selectedOrderArrayList = (ArrayList<SelectedOrderList>) intent.getSerializableExtra("Cloth_Array1");

        /* ArrayList<ClothDataList> clothDataArrayList1 = (ArrayList<ClothDataList>) intent.getSerializableExtra("Cloth_Array1");
        ArrayList<ClothDataList> clothDataArrayList2 = (ArrayList<ClothDataList>) intent.getSerializableExtra("Cloth_Array2");
        ArrayList<ClothDataList> clothDataArrayList3 = (ArrayList<ClothDataList>) intent.getSerializableExtra("Cloth_Array3");*/

        /* Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        ArrayList<ClothDataList> clothDataArrayList1 = (ArrayList<ClothDataList>) bundle.getSerializable("Cloth_Array1");
        ArrayList<ClothDataList> clothDataArrayList2 = (ArrayList<ClothDataList>) bundle.getSerializable("Cloth_Array2");
        ArrayList<ClothDataList> clothDataArrayList3 = (ArrayList<ClothDataList>) bundle.getSerializable("Cloth_Array3");*/

        llPlaceOrder = (LinearLayout) findViewById(R.id.llPlaceOrder);

        tvDate = (TextView) findViewById(R.id.tvDate);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        String currentDateAndTime = sdf.format(new Date());
        tvDate.setText(currentDateAndTime);
        orderDateTime = currentDateAndTime;

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

                //  Log.e("TIME:","H:"+hoursOfDay+" M:"+minuteOfDay+" S:"+secondOfDay);

                datePickerDialog = new DatePickerDialog(OrderListActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                String days = String.valueOf(day);
                                if (days.length() == 1) {
                                    days = "0" + days;
                                }
                                month = month + 1;
                                String months = String.valueOf(month);
                                if (months.length() == 1) {
                                    months = "0" + months;
                                }

                                int hoursOfDay = calendar.get(Calendar.HOUR_OF_DAY);
                                int minuteOfDay = calendar.get(Calendar.MINUTE);
                                int secondOfDay = calendar.get(Calendar.SECOND);

                                String hours = String.valueOf(hoursOfDay);
                                if (hours.length() == 1) {
                                    hours = "0" + hours;
                                }

                                String minute = String.valueOf(minuteOfDay);
                                if (minute.length() == 1) {
                                    minute = "0" + minute;
                                }

                                String second = String.valueOf(secondOfDay);
                                if (second.length() == 1) {
                                    second = "0" + second;
                                }

                                String selectedDate = year + "-" + months + "-" + days + " " + hours + ":" + minute + ":" + second;

                                tvDate.setText(selectedDate);
                                orderDateTime = selectedDate;
                               /* try {
                                    SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date = sdfSource.parse(selectedDate);

                                    SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                                    selectedDate = sdfDestination.format(date);
                                    tvDate.setText(selectedDate);
                                    orderDateTime = selectedDate;

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }*/
                            }
                        }, year, month, dayOfMonth);

                datePickerDialog.show();
            }
        });

        rvOrderList = (RecyclerView) findViewById(R.id.rvOrderList);
        orderListLayoutManager = new LinearLayoutManager(OrderListActivity.this);
        orderListLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvOrderList.setLayoutManager(orderListLayoutManager);
        rvOrderList.setHasFixedSize(true);

        orderDataArrayList = new ArrayList<>();

        for (int i = 0; i < selectedOrderArrayList.size(); i++) {
            SelectedOrderList selectedOrderList = selectedOrderArrayList.get(i);
            orderDataList = new OrderDataList();
            if (Helper.isNetworkAvailable(this)){
                orderDataList.setOrder_url(selectedOrderList.getClothUri());
            }else {
                orderDataList.setOrder_image(mDatabase.getBitmap(selectedOrderList.getCloth_id()));
            }
            orderDataList.setOrder_id(selectedOrderList.getCloth_id());
            orderDataList.setOrder_category(selectedOrderList.getClothCategory());
            orderDataList.setOrder_name(selectedOrderList.getClothName());
            orderDataList.setOrder_quantity(selectedOrderList.getNumber_of_Cloth());
            orderDataArrayList.add(orderDataList);
        }

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Driver", Context.MODE_PRIVATE);
        driver_id = sp.getString("driver_id", "").trim();

        /*for (int i = 0; i<clothDataArrayList1.size(); i++) {
            ClothDataList clothDataList = clothDataArrayList1.get(i);
            String quantity = clothDataList.getNumber_of_Cloth();
            if(Integer.parseInt(quantity) > 0){
                orderDataList = new OrderDataList();
                orderDataList.setOrder_id(clothDataList.getCloth_id());
                orderDataList.setOrder_category(clothDataList.getClothCategory());
                orderDataList.setOrder_name(clothDataList.getClothName());
                orderDataList.setOrder_quantity(clothDataList.getNumber_of_Cloth());
                orderDataList.setOrder_image(clothDataList.getClothBitmap());
                orderDataArrayList.add(orderDataList);
            }
        }

        for (int i = 0; i<clothDataArrayList2.size(); i++) {
            ClothDataList clothDataList = clothDataArrayList2.get(i);
            String quantity = clothDataList.getNumber_of_Cloth();
            if(Integer.parseInt(quantity) > 0){
                orderDataList = new OrderDataList();
                orderDataList.setOrder_id(clothDataList.getCloth_id());
                orderDataList.setOrder_category(clothDataList.getClothCategory());
                orderDataList.setOrder_name(clothDataList.getClothName());
                orderDataList.setOrder_quantity(clothDataList.getNumber_of_Cloth());
                orderDataList.setOrder_image(clothDataList.getClothBitmap());
                orderDataArrayList.add(orderDataList);
            }
        }

        for (int i = 0; i<clothDataArrayList3.size(); i++) {
            ClothDataList clothDataList = clothDataArrayList3.get(i);
            String quantity = clothDataList.getNumber_of_Cloth();
            if(Integer.parseInt(quantity) > 0){
                orderDataList = new OrderDataList();
                orderDataList.setOrder_id(clothDataList.getCloth_id());
                orderDataList.setOrder_category(clothDataList.getClothCategory());
                orderDataList.setOrder_name(clothDataList.getClothName());
                orderDataList.setOrder_quantity(clothDataList.getNumber_of_Cloth());
                orderDataList.setOrder_image(clothDataList.getClothBitmap());
                orderDataArrayList.add(orderDataList);
            }
        }*/

        orderListAdapter = new OrderListAdapter(OrderListActivity.this, orderDataArrayList);

        rvOrderList.setAdapter(orderListAdapter);

        llPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OrderListActivity.this);
                alertDialogBuilder.setMessage("Are you sure you want to place order?");
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if (Helper.user == 0) {
                                    placeOrder();
                                } else if (Helper.user == 1) {
                                    placeIronOrder();
                                }
                            }
                        });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));

            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void placeIronOrder() {

        if (Helper.isNetworkAvailable(OrderListActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("IRON PLACE ORDER:" + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            Intent i = new Intent(OrderListActivity.this, ThankYouActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);

                            /*JSONArray data = object.getJSONArray("result");

                            customerArrayList = new ArrayList<>();

                            names = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String book_id = result.getString("book_id");
                                String user_id = result.getString("user_id");
                                String user_name = result.getString("user_name");


                                customerDataList = new CustomerDataList();
                                customerDataList.setBook_id(book_id);
                                customerDataList.setCustomer_id(user_id);
                                customerDataList.setCustomer_name(user_name);

                                customerArrayList.add(customerDataList);
                                names.add(user_name);
                            }

                            selectCustomerAdapter = new SelectCustomerAdapter( OrderListActivity.this, names, 1);

                            rvCustomerList.setAdapter(selectCustomerAdapter);*/

                        } else {
                            Helper.showToast(OrderListActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(OrderListActivity.this, error);
                    }
                }
            };

            //  JSONObject object=new JSONObject();
            //  object.put("total", String.valueOf(TOTAL_CLOTH_COUNT));
            JSONArray array = new JSONArray();
            for (int i = 0; i < orderDataArrayList.size(); i++) {
                OrderDataList orderDataList = orderDataArrayList.get(i);
                String order_id = orderDataList.getOrder_id();
                String order_quantity = orderDataList.getOrder_quantity();
                JSONObject obj = new JSONObject();
                try {
                    // obj.put("user_id",Helper.CUSTOMER_ID);
                    obj.put("garment_id", order_id);
                    obj.put("no_of_garments", order_quantity);
                    array.put(obj);
                    //    object.put("user_id",obj.toString());
                    Helper.showLog("array:" + array);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            HashMap<String, String> params = new HashMap<>();
            params.put("total", String.valueOf(TOTAL_CLOTH_COUNT));
            params.put("user_garments", array.toString());
            params.put("user_id", IRON_USER_ID);
            params.put("driver_id", driver_id);
            params.put("order_date", orderDateTime);
            Log.e("params", "  " + params);

            API api = new API(OrderListActivity.this, apiResponse);
            api.execute(1, Services.SET_ORDER_IRON, params, true);

        } else {
            Helper.showToast(OrderListActivity.this, Services.NO_NETWORK);
        }
    }

    private void placeOrder() {

        if (Helper.isNetworkAvailable(OrderListActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("PLACE ORDER:" + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            Intent i = new Intent(OrderListActivity.this, ThankYouActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);

                            /*JSONArray data = object.getJSONArray("result");

                            customerArrayList = new ArrayList<>();

                            names = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String book_id = result.getString("book_id");
                                String user_id = result.getString("user_id");
                                String user_name = result.getString("user_name");


                                customerDataList = new CustomerDataList();
                                customerDataList.setBook_id(book_id);
                                customerDataList.setCustomer_id(user_id);
                                customerDataList.setCustomer_name(user_name);

                                customerArrayList.add(customerDataList);
                                names.add(user_name);
                            }

                            selectCustomerAdapter = new SelectCustomerAdapter( OrderListActivity.this, names, 1);

                            rvCustomerList.setAdapter(selectCustomerAdapter);*/

                        } else {
                            Helper.showToast(OrderListActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(OrderListActivity.this, error);
                    }
                }
            };

            //  JSONObject object = new JSONObject();
            //  object.put("total", String.valueOf(TOTAL_CLOTH_COUNT));
            JSONArray array = new JSONArray();
            for (int i = 0; i < orderDataArrayList.size(); i++) {
                OrderDataList orderDataList = orderDataArrayList.get(i);
                String order_id = orderDataList.getOrder_id();
                String order_quantity = orderDataList.getOrder_quantity();
                JSONObject obj = new JSONObject();
                try {
                    // obj.put("user_id",Helper.CUSTOMER_ID);
                    obj.put("garment_id", order_id);
                    obj.put("no_of_garments", order_quantity);
                    array.put(obj);
                    //    object.put("user_id",obj.toString());
                    Helper.showLog("array:" + array);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            HashMap<String, String> params = new HashMap<>();
            params.put("total", String.valueOf(TOTAL_CLOTH_COUNT));
            params.put("user_garments", array.toString());
            params.put("user_id", Helper.CUSTOMER_ID);
            params.put("driver_id", driver_id);
            params.put("order_date", orderDateTime);
            Log.e("params", "  " + params);

            API api = new API(OrderListActivity.this, apiResponse);
            api.execute(1, Services.SET_ORDER, params, true);

        } else {
            Helper.showToast(OrderListActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
