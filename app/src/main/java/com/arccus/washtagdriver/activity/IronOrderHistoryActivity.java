package com.arccus.washtagdriver.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.IronOrderHistoryAdapter;
import com.arccus.washtagdriver.model.OrderHistoryData;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.arccus.washtagdriver.utils.Helper.IRON_USER_ID;

public class IronOrderHistoryActivity extends AppCompatActivity implements IronOrderHistoryAdapter.customButtonListener {

    private RecyclerView rvOrderHistory;
    private LinearLayoutManager orderHistoryLayoutManager;
    private IronOrderHistoryAdapter orderHistoryAdapter;
    private OrderHistoryData orderHistoryData;
    private ArrayList<OrderHistoryData> orderHistoryArrayList;
    private ImageView ivBack;
    private int REQUEST_CODE_UPDATE = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvOrderHistory = (RecyclerView) findViewById(R.id.rvOrderHistory);
        orderHistoryLayoutManager = new LinearLayoutManager(IronOrderHistoryActivity.this);
        orderHistoryLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvOrderHistory.setLayoutManager(orderHistoryLayoutManager);
        rvOrderHistory.setHasFixedSize(true);


        getOrderHistory();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
    }

    private void getOrderHistory(){

        String customer_id = IRON_USER_ID;

        if (Helper.isNetworkAvailable(IronOrderHistoryActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("IRON ORDER LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            orderHistoryArrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String order_no = result.getString("order_no");
                                String order_date = result.getString("order_date");

                                orderHistoryData = new OrderHistoryData();
                                orderHistoryData.setOrder_no(order_no);
                                orderHistoryData.setOrder_date(order_date);

                                orderHistoryArrayList.add(orderHistoryData);
                            }

                            orderHistoryAdapter = new IronOrderHistoryAdapter( IronOrderHistoryActivity.this, orderHistoryArrayList);
                            orderHistoryAdapter.setCustomButtonListener(IronOrderHistoryActivity.this);
                            rvOrderHistory.setAdapter(orderHistoryAdapter);
                            int resId = R.anim.layout_animation_from_bottom;
                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(IronOrderHistoryActivity.this, resId);
                            rvOrderHistory.setLayoutAnimation(animation);

                        } else {
                            Helper.showToast(IronOrderHistoryActivity.this, message);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(IronOrderHistoryActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("iron_user_id", customer_id);

            API api = new API(IronOrderHistoryActivity.this, apiResponse);
            api.execute(1, Services.GET_ORDER_IRON, params, true);

        } else {
            Helper.showToast(IronOrderHistoryActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onButtonClickListener(int position) {
        Intent intent = new Intent(IronOrderHistoryActivity.this, IronOrderDetailsActivity.class);
        intent.putExtra("ORDER_NO", orderHistoryArrayList.get(position).getOrder_no());
        startActivity(intent);
    }

}
