package com.arccus.washtagdriver.activity;

import android.content.Intent;

import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Services;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.fragments.HoseHoldFragment;
import com.arccus.washtagdriver.fragments.KidsFragment;
import com.arccus.washtagdriver.fragments.ManFragment;
import com.arccus.washtagdriver.fragments.WomenFragment;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.model.SelectedOrderList;
import com.arccus.washtagdriver.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.arccus.washtagdriver.utils.Helper.TOTAL_CLOTH_COUNT;

public class ClothTabbedActivity extends AppCompatActivity implements ManFragment.ClothListener,
        WomenFragment.ClothListener, HoseHoldFragment.ClothListener, KidsFragment.ClothListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvItemTotal, tvTotal, tvNext;
    private LinearLayout llNext;

    private ManFragment manFragment;
    private WomenFragment womenFragment;
    private HoseHoldFragment hoseHoldFragment;
    private KidsFragment kidsFragment;

    private SelectedOrderList selectedOrderList;
    ArrayList<SelectedOrderList> selectedOrderArrayList;

    ArrayList<ClothDataList> clothDataArrayList1;
    ArrayList<ClothDataList> clothDataArrayList2;
    ArrayList<ClothDataList> clothDataArrayList3;
    ArrayList<ClothDataList> clothDataArrayList4;

    private ArrayList<ClothDataList> clothDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloth_tabbed);

        TOTAL_CLOTH_COUNT = 0;
        // tvItemTotal = (TextView) findViewById(R.id.tvItemTotal);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvNext = (TextView) findViewById(R.id.tvNext);
        //llNext = (LinearLayout) findViewById(R.id.llNext);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        getClothDetails();
        /*setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);*/

        clothDataArrayList1 = new ArrayList<>();
        clothDataArrayList2 = new ArrayList<>();
        clothDataArrayList3 = new ArrayList<>();
        clothDataArrayList4 = new ArrayList<>();

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Helper.TOTAL_CLOTH_COUNT > 0) {

                    selectedOrderArrayList = new ArrayList<>();

                    for (int i = 0; i < clothDataArrayList1.size(); i++) {
                        ClothDataList clothDataList = clothDataArrayList1.get(i);
                        String quantity = clothDataList.getNumber_of_Cloth();
                        if (Integer.parseInt(quantity) > 0) {
                            selectedOrderList = new SelectedOrderList();
                            selectedOrderList.setCloth_id(clothDataList.getCloth_id());
                            selectedOrderList.setClothCategory(clothDataList.getClothCategory());
                            selectedOrderList.setClothName(clothDataList.getClothName());
                            selectedOrderList.setClothUri(clothDataList.getClothUri());
                            selectedOrderList.setNumber_of_Cloth(clothDataList.getNumber_of_Cloth());
                            selectedOrderArrayList.add(selectedOrderList);
                        }
                    }

                    for (int i = 0; i < clothDataArrayList2.size(); i++) {
                        ClothDataList clothDataList = clothDataArrayList2.get(i);
                        String quantity = clothDataList.getNumber_of_Cloth();
                        if (Integer.parseInt(quantity) > 0) {
                            selectedOrderList = new SelectedOrderList();
                            selectedOrderList.setCloth_id(clothDataList.getCloth_id());
                            selectedOrderList.setClothCategory(clothDataList.getClothCategory());
                            selectedOrderList.setClothName(clothDataList.getClothName());
                            selectedOrderList.setClothUri(clothDataList.getClothUri());
                            selectedOrderList.setNumber_of_Cloth(clothDataList.getNumber_of_Cloth());
                            selectedOrderArrayList.add(selectedOrderList);
                        }
                    }

                    for (int i = 0; i < clothDataArrayList3.size(); i++) {
                        ClothDataList clothDataList = clothDataArrayList3.get(i);
                        String quantity = clothDataList.getNumber_of_Cloth();
                        if (Integer.parseInt(quantity) > 0) {
                            selectedOrderList = new SelectedOrderList();
                            selectedOrderList.setCloth_id(clothDataList.getCloth_id());
                            selectedOrderList.setClothCategory(clothDataList.getClothCategory());
                            selectedOrderList.setClothName(clothDataList.getClothName());
                            selectedOrderList.setClothUri(clothDataList.getClothUri());
                            selectedOrderList.setNumber_of_Cloth(clothDataList.getNumber_of_Cloth());
                            selectedOrderArrayList.add(selectedOrderList);
                        }
                    }

                    for (int i = 0; i < clothDataArrayList4.size(); i++) {
                        ClothDataList clothDataList = clothDataArrayList4.get(i);
                        String quantity = clothDataList.getNumber_of_Cloth();
                        if (Integer.parseInt(quantity) > 0) {
                            selectedOrderList = new SelectedOrderList();
                            selectedOrderList.setCloth_id(clothDataList.getCloth_id());
                            selectedOrderList.setClothCategory(clothDataList.getClothCategory());
                            selectedOrderList.setClothName(clothDataList.getClothName());
                            selectedOrderList.setClothUri(clothDataList.getClothUri());
                            selectedOrderList.setNumber_of_Cloth(clothDataList.getNumber_of_Cloth());
                            selectedOrderArrayList.add(selectedOrderList);
                        }
                    }

                    Intent intent = new Intent(ClothTabbedActivity.this, OrderListActivity.class);
                    intent.putExtra("Cloth_Array1", selectedOrderArrayList);
                    startActivity(intent);

                /*intent.putExtra("Cloth_Array1",clothDataArrayList1);
                intent.putExtra("Cloth_Array2",clothDataArrayList2);
                intent.putExtra("Cloth_Array3",clothDataArrayList3);*/

                /*Bundle bundle = new Bundle();

                bundle.putSerializable("Cloth_Array1",clothDataArrayList1);
                bundle.putSerializable("Cloth_Array2",clothDataArrayList2);
                bundle.putSerializable("Cloth_Array3",clothDataArrayList3);

                intent.putExtras(bundle);*/

                } else {
                    Helper.showToast(ClothTabbedActivity.this, "Please Select Cloth!!");
                }
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        ArrayList<ClothDataList> man = new ArrayList<>();
        ArrayList<ClothDataList> woman = new ArrayList<>();
        ArrayList<ClothDataList> kid = new ArrayList<>();
        ArrayList<ClothDataList> housHold = new ArrayList<>();

        if (clothDataArrayList != null){
            for (int i=0; i<clothDataArrayList.size(); i++){
                if (clothDataArrayList.get(i).getClothCategory().equals("1")){
                    man.add(clothDataArrayList.get(i));
                } else if (clothDataArrayList.get(i).getClothCategory().equals("2")){
                    woman.add(clothDataArrayList.get(i));
                } else if (clothDataArrayList.get(i).getClothCategory().equals("4")){
                    kid.add(clothDataArrayList.get(i));
                } else if (clothDataArrayList.get(i).getClothCategory().equals("3")){
                    housHold.add(clothDataArrayList.get(i));
                }
            }
        }

        adapter.addFragment(manFragment = new ManFragment(man), "Man's");
        adapter.addFragment(womenFragment = new WomenFragment(woman), "Women's");
        adapter.addFragment(kidsFragment = new KidsFragment(kid), "Kid's");
        adapter.addFragment(hoseHoldFragment = new HoseHoldFragment(housHold), "Household");
        viewPager.setAdapter(adapter);

        manFragment.setListener(ClothTabbedActivity.this);
        womenFragment.setListener(ClothTabbedActivity.this);
        kidsFragment.setListener(ClothTabbedActivity.this);
        hoseHoldFragment.setListener(ClothTabbedActivity.this);
    }

    private void getClothDetails() {

        if (Helper.isNetworkAvailable(ClothTabbedActivity.this)) {
            // mDatabase = new DatabaseHelper(SelectCustomerActivity.this);
            clothDataArrayList = new ArrayList<>();
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("GET CLOTH : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {


                            JSONArray data = object.getJSONArray("result");

                            Log.e("TOTAL CLOTH NUMBER:", "" + data.length());
                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String garment_id = result.getString("garment_id");
                                String garment_category_id = result.getString("garment_category_id");
                                String garment_category_name = result.getString("garment_category_name");
                                String garment_name = result.getString("garment_name");
                                String garment_image = result.getString("garment_image");

                                ClothDataList clothDataList = new ClothDataList();
                                clothDataList.setCloth_id(garment_id);
                                clothDataList.setClothCategory(garment_category_id);
                                clothDataList.setClothName(garment_name);
                                clothDataList.setNumber_of_Cloth("0");
                                clothDataList.setClothUri(garment_image);
                                clothDataList.setClothBitmap(null);

                                clothDataArrayList.add(clothDataList);

                            }

                            //   Helper.showToast(LoginActivity.this, message);
                            /* Intent intent = new Intent(SelectCustomerActivity.this, OtpVerificationActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/

                        } else {
                            Helper.showToast(ClothTabbedActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    setupViewPager(viewPager);

                    tabLayout = (TabLayout) findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(viewPager);
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(ClothTabbedActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(ClothTabbedActivity.this, apiResponse);
            api.execute(1, Services.GET_CLOTH, params, true);
        } else {
            Helper.showToast(ClothTabbedActivity.this, Services.NO_NETWORK);
            setupViewPager(viewPager);

            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    @Override
    public void getCloth1(ArrayList<ClothDataList> clothDataArrayList) {

        clothDataArrayList1 = new ArrayList<>();
        this.clothDataArrayList1 = clothDataArrayList;
        totalCloth();
        // tvItemTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
        tvTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
    }

    @Override
    public void getCloth2(ArrayList<ClothDataList> clothDataArrayList) {
        clothDataArrayList2 = new ArrayList<>();
        this.clothDataArrayList2 = clothDataArrayList;
        totalCloth();
        // tvItemTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
        tvTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
    }

    @Override
    public void getCloth3(ArrayList<ClothDataList> clothDataArrayList) {
        clothDataArrayList3 = new ArrayList<>();
        this.clothDataArrayList3 = clothDataArrayList;
        totalCloth();

        // tvItemTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
        tvTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
    }

    @Override
    public void getCloth4(ArrayList<ClothDataList> clothDataArrayList) {
        clothDataArrayList4 = new ArrayList<>();
        this.clothDataArrayList4 = clothDataArrayList;
        totalCloth();
        // tvItemTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
        tvTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void totalCloth() {
        int tot = 0;
        if (this.clothDataArrayList1 != null && this.clothDataArrayList1.size()>0){
            for (int t=0; t<clothDataArrayList1.size(); t++){
                tot = tot + Integer.parseInt(clothDataArrayList1.get(t).getNumber_of_Cloth());
            }
        }

        if (this.clothDataArrayList2 != null && this.clothDataArrayList2.size()>0){
            for (int t=0; t<clothDataArrayList2.size(); t++){
                tot = tot + Integer.parseInt(clothDataArrayList2.get(t).getNumber_of_Cloth());
            }
        }

        if (this.clothDataArrayList3 != null && this.clothDataArrayList3.size()>0){
            for (int t=0; t<clothDataArrayList3.size(); t++){
                tot = tot + Integer.parseInt(clothDataArrayList3.get(t).getNumber_of_Cloth());
            }
        }

        if (this.clothDataArrayList4 != null && this.clothDataArrayList4.size()>0){
            for (int t=0; t<clothDataArrayList4.size(); t++){
                tot = tot + Integer.parseInt(clothDataArrayList4.get(t).getNumber_of_Cloth());
            }
        }
        TOTAL_CLOTH_COUNT = tot;
        tvTotal.setText(String.valueOf(TOTAL_CLOTH_COUNT));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        TOTAL_CLOTH_COUNT = 0;
       /* Intent intent = new Intent(ClothTabbedActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);*/
    }
}
