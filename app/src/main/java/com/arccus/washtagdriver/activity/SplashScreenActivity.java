package com.arccus.washtagdriver.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class SplashScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    private ImageView ivSplash;
    private DatabaseHelper mDatabase;
    private ClothDataList clothDataList;

    private ArrayList<ClothDataList> clothDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ivSplash = (ImageView) findViewById(R.id.ivSplash);
        mDatabase = new DatabaseHelper(this);
        /*Glide.with(SplashScreenActivity.this)
                .load(R.drawable.splash_screen)
                .into(ivSplash);*/

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                SharedPreferences sp = getApplicationContext().getSharedPreferences("Driver", Context.MODE_PRIVATE);
                String user_id = sp.getString("driver_id", "").trim();

                if(user_id.length()>0) {
                    Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);

//        getClothDetails();
    }

    private void Next(){
        SharedPreferences sp = getApplicationContext().getSharedPreferences("Driver", Context.MODE_PRIVATE);
        String user_id = sp.getString("driver_id", "").trim();

        if(user_id.length()>0) {
            Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    }

    private void getClothDetails() {

        if (Helper.isNetworkAvailable(SplashScreenActivity.this)) {
            // mDatabase = new DatabaseHelper(SelectCustomerActivity.this);
            clothDataArrayList = new ArrayList<>();
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("GET CLOTH : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            mDatabase.deleteDatabase();

                            JSONArray data = object.getJSONArray("result");

                            Log.e("TOTAL CLOTH NUMBER:", "" + data.length());
                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String garment_id = result.getString("garment_id");
                                String garment_category_id = result.getString("garment_category_id");
                                String garment_category_name = result.getString("garment_category_name");
                                String garment_name = result.getString("garment_name");
                                String garment_image = result.getString("garment_image");

                                clothDataList = new ClothDataList();
                                clothDataList.setCloth_id(garment_id);
                                clothDataList.setClothCategory(garment_category_id);
                                clothDataList.setClothName(garment_name);
                                clothDataList.setNumber_of_Cloth("0");
                                clothDataList.setClothUri(garment_image);
                                clothDataList.setClothBitmap(null);

                                clothDataArrayList.add(clothDataList);
                            }

                            SaveIntoDatabase task = new SaveIntoDatabase();
                            task.execute();

                            //   Helper.showToast(LoginActivity.this, message);

                            /* Intent intent = new Intent(SelectCustomerActivity.this, OtpVerificationActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/

                        } else {
                            Helper.showToast(SplashScreenActivity.this, message);
//                            Next();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Next();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(SplashScreenActivity.this, error);
//                        Next();
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(SplashScreenActivity.this, apiResponse);
            api.execute(1, Services.GET_CLOTH, params, false);
        } else {
            Helper.showToast(SplashScreenActivity.this, Services.NO_NETWORK);
//            Next();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class SaveIntoDatabase extends AsyncTask<Void, Void, Void> {
        // can use UI thread here
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        // automatically done on worker thread (separate from UI thread)
        @Override
        protected Void doInBackground(Void... params) {
            //  ClothDataList dataModel = params[0];
            //  ClothDataList clothDataList1 = new ClothDataList();

            for (int i = 0; i < clothDataArrayList.size(); i++) {
                ClothDataList dataModel = clothDataArrayList.get(i);

                try {
                    ClothDataList clothDataList1 = new ClothDataList();

                    String image_url = dataModel.getClothUri();
                    InputStream inputStream = new URL(image_url).openStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                    clothDataList1.setCloth_id(dataModel.getCloth_id());
                    clothDataList1.setClothName(dataModel.getClothName());
                    clothDataList1.setNumber_of_Cloth("0");
                    clothDataList1.setClothCategory(dataModel.getClothCategory());
                    clothDataList1.setClothBitmap(bitmap);
                    //add data to database (shows in next step)
                    mDatabase.addData(clothDataList1);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            Next();
        }
    }

}
