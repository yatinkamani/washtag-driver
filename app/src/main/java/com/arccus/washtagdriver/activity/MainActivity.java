package com.arccus.washtagdriver.activity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;

import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.fragments.HomeFragment;
import com.arccus.washtagdriver.fragments.OrderFragment;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private FrameLayout content_frame;
    private NavigationView navigationView;
    private View navHeader;
    private TextView tvUserName;
    private ImageView ivLogout;

    private DatabaseHelper mDatabase;
    private ClothDataList clothDataList;

    private ArrayList<ClothDataList> clothDataArrayList;
    private ProgressDialog prd;

    int home_icon = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mDatabase = new DatabaseHelper(MainActivity.this);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        content_frame = findViewById(R.id.content_frame);

        navigationView = findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);
        tvUserName = (TextView) navHeader.findViewById(R.id.tvuserName);
        ivLogout = (ImageView) navHeader.findViewById(R.id.ivLogout);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Driver", Context.MODE_PRIVATE);
        String user_name = sp.getString("driver_name", "").trim();
        // String user_name = "User Name";/
        tvUserName.setText(user_name);

       /* getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame,new SelectCustomerFragment())
                .commit();*/

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new HomeFragment())
                .commit();

        Helper.CUSTOMER_NAME = "";

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
//                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch (menuItem.getItemId()) {
                            case R.id.nav_home:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, new HomeFragment())
                                        .commit();
                                break;
                            case R.id.nav_profile:
                                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                                /* ActivityOptions options = ActivityOptions.makeScaleUpAnimation(content_frame, 0,
                                        0, content_frame.getWidth(), content_frame.getHeight());*/
                                startActivity(intent);
                                break;

                            case R.id.nav_my_order:
                                // startActivity(new Intent(MainActivity.this, SelectCustomerActivity.class));
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, new OrderFragment())
                                        .addToBackStack(null)
                                        .commit();
                                break;

                            case R.id.nav_my_order_iron:
                                startActivity(new Intent(MainActivity.this, IronOrderHistoryActivity.class));
                                break;

                            case R.id.nav_logout:
                                logout();
                                break;

                            case R.id.nav_refresh:
                                getClothDetails();
                                break;

                            /*case R.id.nav_contact_us:
                                Intent intent_contact = new Intent(MainActivity.this, ContactUsActivity.class);
                                startActivity(intent_contact);*/
                               /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://kaprat.com/privacypolicy.html"));
                                startActivity(browserIntent);*//*
                                break;*/

                            case R.id.nav_rate_us:
                                launchMarket();
                                break;

                            case R.id.nav_share:
                                shareApp();
                                break;
                        }
                        return true;
                    }
                });

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }


    public void setActionBarTitle(String title, int b) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(b);
        home_icon = b;
    }

    private void getClothDetails() {

        if (Helper.isNetworkAvailable(MainActivity.this)) {
            // mDatabase = new DatabaseHelper(SelectCustomerActivity.this);
            clothDataArrayList = new ArrayList<>();
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("GET CLOTH : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            mDatabase.deleteDatabase();

                            JSONArray data = object.getJSONArray("result");

                            Log.e("TOTAL CLOTH NUMBER:", "" + data.length());
                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String garment_id = result.getString("garment_id");
                                String garment_category_id = result.getString("garment_category_id");
                                String garment_category_name = result.getString("garment_category_name");
                                String garment_name = result.getString("garment_name");
                                String garment_image = result.getString("garment_image");

                                clothDataList = new ClothDataList();
                                clothDataList.setCloth_id(garment_id);
                                clothDataList.setClothCategory(garment_category_id);
                                clothDataList.setClothName(garment_name);
                                clothDataList.setNumber_of_Cloth("0");
                                clothDataList.setClothUri(garment_image);
                                clothDataList.setClothBitmap(null);

                                clothDataArrayList.add(clothDataList);
                            }

                            SaveIntoDatabase task = new SaveIntoDatabase();
                            task.execute();

                            //   Helper.showToast(LoginActivity.this, message);

                            /* Intent intent = new Intent(SelectCustomerActivity.this, OtpVerificationActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/

                        } else {
                            Helper.showToast(MainActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(MainActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(MainActivity.this, apiResponse);
            api.execute(1, Services.GET_CLOTH, params, true);
        } else {
            Helper.showToast(MainActivity.this, Services.NO_NETWORK);
        }
    }

    public class SaveIntoDatabase extends AsyncTask<Void, Void, Void> {
        // can use UI thread here
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showLoader();
        }

        // automatically done on worker thread (separate from UI thread)
        @Override
        protected Void doInBackground(Void... params) {
            //  ClothDataList dataModel = params[0];
            //  ClothDataList clothDataList1 = new ClothDataList();

            for (int i = 0; i < clothDataArrayList.size(); i++) {
                ClothDataList dataModel = clothDataArrayList.get(i);

                try {
                    ClothDataList clothDataList1 = new ClothDataList();

                    String image_url = dataModel.getClothUri();
                    InputStream inputStream = new URL(image_url).openStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                    clothDataList1.setCloth_id(dataModel.getCloth_id());
                    clothDataList1.setClothName(dataModel.getClothName());
                    clothDataList1.setNumber_of_Cloth("0");
                    clothDataList1.setClothCategory(dataModel.getClothCategory());
                    clothDataList1.setClothBitmap(bitmap);
                    //add data to database (shows in next step)
                    mDatabase.addData(clothDataList1);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            dismissLoader();
        }
    }

    private void showLoader() {
        prd = new ProgressDialog(MainActivity.this);
        prd.setMessage("Please, Wait a moment");
        prd.setTitle("Processing...");
        prd.setCancelable(false);
        prd.show();
    }

    private void dismissLoader() {
        if (prd != null && prd.isShowing())
            prd.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (home_icon == R.drawable.ic_arrow_back) {
                    onBackPressed();
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this app at: https://play.google.com/store/apps/details?id=" + MainActivity.this.getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + MainActivity.this.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(MainActivity.this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    private void logout() {
        mDatabase.deleteDatabase();
        SharedPreferences sp = getApplicationContext().getSharedPreferences("Driver", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        try {
            if (mDrawerLayout.isDrawerOpen(navigationView))
                mDrawerLayout.closeDrawer(navigationView);
            else
                super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
