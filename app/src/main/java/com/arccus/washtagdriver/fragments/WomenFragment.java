package com.arccus.washtagdriver.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.ClothAdapter;
import com.arccus.washtagdriver.database.DataFetchListner;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.helper.ClothItemClickListener;
import com.arccus.washtagdriver.model.ClothDataList;

import java.util.ArrayList;


public class WomenFragment extends Fragment implements ClothItemClickListener, DataFetchListner {

    private RecyclerView rvCloth;
    private LinearLayoutManager clothLayoutManager;
    private ClothAdapter clothAdapter;
    private ArrayList<ClothDataList> clothDataArrayList;
    private ClothListener clothListener;
    private DatabaseHelper mDatabase;

   /* private int[] cloth_int = {R.drawable.birthday_icon1, R.drawable.birthday_icon2, R.drawable.birthday_icon3,
            R.drawable.birthday_icon4, R.drawable.birthday_icon5, R.drawable.birthday_icon6, R.drawable.birthday_icon7,
            R.drawable.birthday_icon8, R.drawable.birthday_icon9, R.drawable.birthday_icon10};

    private String[] cloth_id = {"11", "12", "13", "14", "15", "16", "17", "18", "19", "110"};
    private String[] cloth_name = {"a1", "b2", "c3", "d4", "e5", "f6", "g7", "h8", "i9", "j0"};
    private String[] noof_cloth = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};
    private String[] cloth_category = {"2", "2", "2", "2", "2", "2", "2", "2", "2", "2"};
*/
    public void setListener(ClothListener clothListener) {
        this.clothListener = clothListener;
    }

    public WomenFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public WomenFragment(ArrayList<ClothDataList> dataLists) {
        this.clothDataArrayList = dataLists;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("WOMEN","True");
        View view = inflater.inflate(R.layout.fragment_women, container, false);

        mDatabase = new DatabaseHelper(getActivity());

        rvCloth = (RecyclerView)view.findViewById(R.id.rvCloth);
        clothLayoutManager = new LinearLayoutManager(getActivity());
        clothLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCloth.setLayoutManager(clothLayoutManager);
        rvCloth.setHasFixedSize(true);

        if (clothDataArrayList != null && clothDataArrayList.size() > 0) {
            clothAdapter = new ClothAdapter(clothDataArrayList, WomenFragment.this, getActivity());
            rvCloth.setAdapter(clothAdapter);
        } else {
            getFeedFromDatabase();
        }

        return view;
    }

    private void getFeedFromDatabase() {
        clothDataArrayList = new ArrayList<>();
        mDatabase.fetchData(WomenFragment.this, "2");
    }

    @Override
    public void onDeliverData(ArrayList<ClothDataList> clothDataArrayList) {
        this.clothDataArrayList = clothDataArrayList;
        clothAdapter = new ClothAdapter(clothDataArrayList, WomenFragment.this,getActivity());
        rvCloth.setAdapter(clothAdapter);
    }

    @Override
    public void onHideDialog() {

    }

    @Override
    public void onclothItemClick(ArrayList<ClothDataList> clothDataArrayList) {

        clothListener.getCloth2(clothDataArrayList);
    }

    public interface ClothListener {
        void getCloth2(ArrayList<ClothDataList> clothDataArrayList);
    }
}
