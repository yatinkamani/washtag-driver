package com.arccus.washtagdriver.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.ClothAdapter;
import com.arccus.washtagdriver.database.DataFetchListner;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.helper.ClothItemClickListener;
import com.arccus.washtagdriver.model.ClothDataList;

import java.util.ArrayList;

public class KidsFragment extends Fragment implements ClothItemClickListener, DataFetchListner {

    private RecyclerView rvCloth;
    private LinearLayoutManager clothLayoutManager;
    private ClothAdapter clothAdapter;
    private ClothDataList clothDataList;
    private ArrayList<ClothDataList> clothDataArrayList;
    private ClothListener clothListener;
    private DatabaseHelper mDatabase;

    public void setListener(ClothListener clothListener) {
        this.clothListener = clothListener;
    }

    public KidsFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public KidsFragment(ArrayList<ClothDataList> dataLists) {
        this.clothDataArrayList = dataLists;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.e("KIDS", "True");
        View view = inflater.inflate(R.layout.fragment_kids, container, false);

        mDatabase = new DatabaseHelper(getActivity());

        rvCloth = (RecyclerView) view.findViewById(R.id.rvCloth);
        clothLayoutManager = new LinearLayoutManager(getActivity());
        clothLayoutManager.scrollToPosition(0);
        // overlayLayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCloth.setLayoutManager(clothLayoutManager);
        rvCloth.setHasFixedSize(true);

        if (clothDataArrayList != null && clothDataArrayList.size() > 0) {
            clothAdapter = new ClothAdapter(clothDataArrayList, KidsFragment.this, getActivity());
            rvCloth.setAdapter(clothAdapter);
        } else {
            getFeedFromDatabase();
        }

        return view;
    }


    private void getFeedFromDatabase() {
        clothDataArrayList = new ArrayList<>();
        mDatabase.fetchData(KidsFragment.this, "4");
    }

    @Override
    public void onclothItemClick(ArrayList<ClothDataList> clothDataArrayList) {

        clothListener.getCloth4(clothDataArrayList);
    }

    @Override
    public void onDeliverData(ArrayList<ClothDataList> clothDataArrayList) {
        this.clothDataArrayList = clothDataArrayList;
        clothAdapter = new ClothAdapter(clothDataArrayList, KidsFragment.this, getActivity());
        rvCloth.setAdapter(clothAdapter);
    }

    @Override
    public void onHideDialog() {

    }


    public interface ClothListener {
        void getCloth4(ArrayList<ClothDataList> clothDataArrayList);
    }
}
