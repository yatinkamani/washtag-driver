package com.arccus.washtagdriver.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.activity.MainActivity;
import com.arccus.washtagdriver.adapter.SelectCustomerAdapter;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.model.CustomerDataList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.arccus.washtagdriver.utils.Helper.CUSTOMER_ID;

public class SelectCustomerFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private RecyclerView rvCustomerList;
    private LinearLayoutManager customerLayoutManager;
    private SelectCustomerAdapter selectCustomerAdapter;
    private CustomerDataList customerDataList;
    private ArrayList<CustomerDataList> customerArrayList;
    private ImageView ivSearch;

    private RelativeLayout rlMain;
    private String driver_id;
    EditText editTextSearch;
    ArrayList<String> names;

    private DatabaseHelper mDatabase;
    private ClothDataList clothDataList;

    public SelectCustomerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_customer, container, false);

        Helper.CUSTOMER_NAME = "";
        CUSTOMER_ID = "";

        ((MainActivity)getActivity()).setActionBarTitle("User List",R.drawable.ic_arrow_back);
        SharedPreferences sp = getActivity().getSharedPreferences("Driver", Context.MODE_PRIVATE);
        driver_id = sp.getString("driver_id", "").trim();

        rlMain = (RelativeLayout) view.findViewById(R.id.rlMain);
        editTextSearch = (EditText) view.findViewById(R.id.editTextSearch);
        ivSearch = (ImageView) view.findViewById(R.id.ivSearch);

        rvCustomerList = (RecyclerView) view.findViewById(R.id.rvCustomerList);
        customerLayoutManager = new LinearLayoutManager(getActivity());
        customerLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCustomerList.setLayoutManager(customerLayoutManager);
        rvCustomerList.setHasFixedSize(true);

        getCustomerList();

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }

        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.requestFocus();
                String text = editTextSearch.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    Helper.showToast(getActivity(), "Please enter name");
                } else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rlMain.getWindowToken(), 0);
                }
            }
        });

        return view;
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<String> filterNames = new ArrayList<>();

        if (names.size() > 0) {
            //looping through existing elements
            for (String s : names) {
                //if the existing elements contains the search input
                if (s.toLowerCase().contains(text.toLowerCase())) {
                    //adding the element to filtered list
                    filterNames.add(s);
                }
            }

            //calling a method of the adapter class and passing the filtered list
            selectCustomerAdapter.filterList(filterNames);
        }
    }

    private void getCustomerList() {

        names = new ArrayList<>();
        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("CUSTOMER LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");


                            customerArrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                //   String book_id = result.getString("book_id");
                                String user_id = result.getString("user_id");
                                String user_name = result.getString("user_name");
                                String customer_no = result.getString("customer_no");

                                customerDataList = new CustomerDataList();
                                //  customerDataList.setBook_id(book_id);
                                customerDataList.setCustomer_id(user_id);
                                customerDataList.setCustomer_name(customer_no + "." + user_name);

                                customerArrayList.add(customerDataList);
                                names.add(customer_no + "." + user_name);
                            }

                            if (customerArrayList.size() > 0 && names.size() > 0) {
                                selectCustomerAdapter = new SelectCustomerAdapter(getActivity(), names, customerArrayList, 0);

                                rvCustomerList.setAdapter(selectCustomerAdapter);

                                int resId = R.anim.layout_animation_from_bottom;
                                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
                                rvCustomerList.setLayoutAnimation(animation);
                            }

                        } else {
                            Helper.showToast(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", driver_id);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_ACTIVE_CUSTOMER_LIST, params, true);

        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    interface OnFragmentInteractionListener {

    }
}
