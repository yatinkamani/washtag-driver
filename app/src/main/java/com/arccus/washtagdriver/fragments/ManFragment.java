package com.arccus.washtagdriver.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.adapter.ClothAdapter;
import com.arccus.washtagdriver.database.DataFetchListner;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.helper.ClothItemClickListener;
import com.arccus.washtagdriver.model.ClothDataList;

import java.util.ArrayList;


public class ManFragment extends Fragment implements ClothItemClickListener, DataFetchListner {

    private RecyclerView rvCloth;
    private LinearLayoutManager clothLayoutManager;
    private ClothAdapter clothAdapter;
    private ClothDataList clothDataList;
    private ArrayList<ClothDataList> clothDataArrayList;
    private ClothListener clothListener;
    private DatabaseHelper mDatabase;

    /*private int[] cloth_int = {R.drawable.birthday_icon1, R.drawable.birthday_icon2, R.drawable.birthday_icon3,
            R.drawable.birthday_icon4, R.drawable.birthday_icon5, R.drawable.birthday_icon6, R.drawable.birthday_icon7,
            R.drawable.birthday_icon8, R.drawable.birthday_icon9, R.drawable.birthday_icon10};

    private String[] cloth_id = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    private String[] cloth_name = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
    private String[] noof_cloth = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};
    private String[] cloth_category = {"1", "1", "1", "1", "1", "1", "1", "1", "1", "1"};*/

    public void setListener(ClothListener clothListener) {
        this.clothListener = clothListener;
    }

    public ManFragment() {
    }

    @SuppressLint("ValidFragment")
    public ManFragment(ArrayList<ClothDataList> dataLists) {
        this.clothDataArrayList = dataLists;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.e("MAN", "True");
        View view = inflater.inflate(R.layout.fragment_man, container, false);

        mDatabase = new DatabaseHelper(getActivity());

        rvCloth = (RecyclerView) view.findViewById(R.id.rvCloth);
        clothLayoutManager = new LinearLayoutManager(getActivity());
        clothLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCloth.setLayoutManager(clothLayoutManager);
        rvCloth.setHasFixedSize(true);

        /* for (int oid : cloth_int){
            clothDataList = new ClothDataList();
            clothDataList.setCloth_id();
            clothDataArrayList.add(overlayList);
        }*/

        /*for(int i = 0; i<cloth_id.length; i++){
            clothDataList = new ClothDataList();
            clothDataList.setCloth_id(cloth_id[i]);
            clothDataList.setNumber_of_Cloth(noof_cloth[i]);
            clothDataList.setClothCategory(cloth_category[i]);
            clothDataList.setClothInt(cloth_int[i]);
            clothDataList.setClothName(cloth_name[i]);
            clothDataArrayList.add(clothDataList);
        }*/

        if (clothDataArrayList != null && clothDataArrayList.size() > 0) {
            clothAdapter = new ClothAdapter(clothDataArrayList, ManFragment.this, getActivity());
            rvCloth.setAdapter(clothAdapter);
        } else {
            getFeedFromDatabase();
        }

        return view;
    }

    private void getFeedFromDatabase() {
        clothDataArrayList = new ArrayList<>();
        mDatabase.fetchData(ManFragment.this, "1");
    }

    @Override
    public void onDeliverData(ArrayList<ClothDataList> clothDataArrayList) {
        this.clothDataArrayList = clothDataArrayList;
        clothAdapter = new ClothAdapter(clothDataArrayList, ManFragment.this, getActivity());
        rvCloth.setAdapter(clothAdapter);
    }

    @Override
    public void onHideDialog() {

    }

    @Override
    public void onclothItemClick(ArrayList<ClothDataList> clothDataArrayList) {

        clothListener.getCloth1(clothDataArrayList);
    }


    public interface ClothListener {
        void getCloth1(ArrayList<ClothDataList> clothDataArrayList);
    }
}
