package com.arccus.washtagdriver.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.activity.MainActivity;
import com.arccus.washtagdriver.activity.SelectIronUserActivity;
import com.arccus.washtagdriver.utils.Helper;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IndividualFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link IndividualFragment newInstance} factory method to
 * create an instance of this fragment.
 */
public class IndividualFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LinearLayout llDryClean, llWashIron, llCarWash, llHotelWash, llHospitalWash, llHostelWash, llStreamPress, llWashPress;

    private TextView txt_washFoldSteam;

    private ScrollView scrollView;

    private List<LinearLayout> layoutList = new ArrayList<>();

    public IndividualFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_individual, container, false);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        llDryClean = (LinearLayout) view.findViewById(R.id.llDryClean);
        llWashIron = (LinearLayout) view.findViewById(R.id.llWashIron);
        llCarWash = (LinearLayout) view.findViewById(R.id.llCarWash);
        llHotelWash = (LinearLayout) view.findViewById(R.id.llHotelWash);
        llHospitalWash = (LinearLayout) view.findViewById(R.id.llHospitalWash);
        llHostelWash = (LinearLayout) view.findViewById(R.id.llHostelWash);
        llStreamPress = (LinearLayout) view.findViewById(R.id.llStreamPress);
        llWashPress = (LinearLayout) view.findViewById(R.id.llWashPress);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        txt_washFoldSteam = (TextView) view.findViewById(R.id.txt_washFoldSteam);
        ((MainActivity) getActivity()).setActionBarTitle("Individual", R.drawable.ic_arrow_back);

        layoutList.add(llCarWash);
        layoutList.add(llDryClean);
        layoutList.add(llHospitalWash);
        layoutList.add(llHostelWash);
        layoutList.add(llHotelWash);
        layoutList.add(llStreamPress);
        layoutList.add(llWashIron);
        layoutList.add(llWashPress);

        llDryClean.setOnClickListener(this);
        llWashIron.setOnClickListener(this);
        llCarWash.setOnClickListener(this);
        llHotelWash.setOnClickListener(this);
        llHospitalWash.setOnClickListener(this);
        llHostelWash.setOnClickListener(this);
        llStreamPress.setOnClickListener(this);
        llWashPress.setOnClickListener(this);

        txt_washFoldSteam.setSelected(true);
        txt_washFoldSteam.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        txt_washFoldSteam.setMarqueeRepeatLimit(2);
        setLinCardElevation(layoutList, getViews());

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == llWashIron) {
            setLinCardElevation(layoutList, llWashIron);
            Helper.INDIVIDUAL_SERVICE_NAME = "Wash & Fold";
        } else if (view == llDryClean) {
            setLinCardElevation(layoutList, llDryClean);
            Helper.INDIVIDUAL_SERVICE_NAME = "Dry Clean";
        } else if (view == llStreamPress) {
            setLinCardElevation(layoutList, llStreamPress);
            Helper.INDIVIDUAL_SERVICE_NAME = "Steam Press";
        } else if (view == llWashPress) {
            setLinCardElevation(layoutList, llWashPress);
            Helper.INDIVIDUAL_SERVICE_NAME = "Wash & Press";
        } else if (view == llCarWash) {
            setLinCardElevation(layoutList, llCarWash);
            Helper.INDIVIDUAL_SERVICE_NAME = "Car Wash";
        } else if (view == llHotelWash) {
            setLinCardElevation(layoutList, llHotelWash);
            Helper.INDIVIDUAL_SERVICE_NAME = "Hotel Wash";
        } else if (view == llHostelWash) {
            setLinCardElevation(layoutList, llHostelWash);
            Helper.INDIVIDUAL_SERVICE_NAME = "Hostel Wash";
        } else if (view == llHospitalWash) {
            setLinCardElevation(layoutList, llHospitalWash);
            Helper.INDIVIDUAL_SERVICE_NAME = "Hospital Wash";
        }

        if (Helper.status == 0) {
            Intent intent = new Intent(getActivity(), SelectIronUserActivity.class);
            startActivity(intent);
        } else {
            Helper.user = 1;
            Fragment someFragment = new IronFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, someFragment); // give your fragment container id in first parameter
            transaction.addToBackStack(null);  // if written, this transaction will be added to backStack
            transaction.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        /*setLinCardElevation(llWashIron, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llDryClean, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llCarWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llHotelWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llHospitalWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llHostelWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llWashPress, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llStreamPress, 6f, getResources().getColor(R.color.home_item_color));*/
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void setLinCardElevation(List<LinearLayout> layout, LinearLayout d_layout) {

        if (d_layout != null){
            for (int i = 0; i < layout.size(); i++) {

                CardView cv = (CardView) layout.get(i).getParent();
                if (layout.get(i).getId() == d_layout.getId()) {
                    cv.setCardElevation(20f);
                    cv.setCardBackgroundColor(getResources().getColor(R.color.home_item_color_dark));
                } else {
                    cv.setCardElevation(6f);
                    cv.setCardBackgroundColor(getResources().getColor(R.color.home_item_color));
                }
            }
        }
    }

    private LinearLayout getViews() {

        switch (Helper.INDIVIDUAL_SERVICE_NAME) {
            case "Wash & Fold":
                return llWashIron;
            case "Dry Clean":
                return llDryClean;
            case "Steam Press":
                return llStreamPress;
            case "Wash & Press":
                return llWashPress;
            case "Car Wash":
                return llCarWash;
            case "Hotel Wash":
                return llHotelWash;
            case "Hostel Wash":
                return llHostelWash;
            case "Hospital Wash":
                return llHospitalWash;
            default:
                return null;
        }
    }
}
