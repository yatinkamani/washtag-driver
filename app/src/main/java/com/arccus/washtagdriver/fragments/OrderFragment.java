package com.arccus.washtagdriver.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.activity.MainActivity;
import com.arccus.washtagdriver.activity.SelectCustomerActivity;
import com.arccus.washtagdriver.activity.SelectIronUserActivity;
import com.arccus.washtagdriver.utils.Helper;


public class OrderFragment extends Fragment {

    private LinearLayout llIron;
    LinearLayout llWash;

    public OrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        ((MainActivity)getActivity()).setActionBarTitle(getString(R.string.app_name), R.drawable.ic_menu);
        llIron = (LinearLayout) view.findViewById(R.id.llIron);
        llWash = (LinearLayout) view.findViewById(R.id.llWash);

        llIron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.status = 0;
                /*Intent intent = new Intent(getActivity(), SelectIronUserActivity.class);
                startActivity(intent);*/
                Fragment someFragment = new IndividualFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, someFragment ); // give your fragment container id in first parameter
                transaction.addToBackStack(null);  // if written, this transaction will be added to backStack
                transaction.commit();
            }
        });

        llWash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SelectCustomerActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    public interface OnFragmentInteractionListener {

    }

}
