package com.arccus.washtagdriver.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.activity.MainActivity;
import com.arccus.washtagdriver.utils.Helper;

public class HomeFragment extends Fragment {

    private LinearLayout llIron, llWash;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // enter your name enter your mobile

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        ((MainActivity)getActivity()).setActionBarTitle(getString(R.string.app_name), R.drawable.ic_menu);

        llIron = (LinearLayout) view.findViewById(R.id.llIron);
        llWash = (LinearLayout) view.findViewById(R.id.llWash);
        Helper.INDIVIDUAL_SERVICE_NAME = "";
        llIron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.user = 1;
                Helper.status = 1;
                Fragment someFragment = new IndividualFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, someFragment ); // give your fragment container id in first parameter
                transaction.addToBackStack(null);  // if written, this transaction will be added to backStack
                transaction.commit();
            }
        });

        llWash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.user = 0;
                Fragment someFragment = new SelectCustomerFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, someFragment ); // give your fragment container id in first parameter
                transaction.addToBackStack(null);  // if written, this transaction will be added to backStack
                transaction.commit();
            }
        });

        return view;
    }

    interface OnFragmentInteractionListener {

    }

}
