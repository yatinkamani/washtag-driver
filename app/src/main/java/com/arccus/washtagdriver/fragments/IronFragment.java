package com.arccus.washtagdriver.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.activity.MainActivity;
import com.arccus.washtagdriver.adapter.SelectIronUserAdapter;
import com.arccus.washtagdriver.database.DatabaseHelper;
import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.model.CustomerDataList;
import com.arccus.washtagdriver.utils.API;
import com.arccus.washtagdriver.utils.APIResponse;
import com.arccus.washtagdriver.utils.Helper;
import com.arccus.washtagdriver.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.text.TextUtils.isEmpty;
import static com.arccus.washtagdriver.utils.Helper.IRON_USER_ID;

public class IronFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private RecyclerView rvCustomerList;
    private LinearLayoutManager customerLayoutManager;
    private SelectIronUserAdapter selectCustomerAdapter;
    private CustomerDataList customerDataList;
    private ArrayList<CustomerDataList> customerArrayList;
    private ImageView ivSearch;
    private ImageView ivAdd;

    private RelativeLayout rlMain;
    EditText editTextSearch;
    private String driver_id;
    ArrayList<String> names;

    private DatabaseHelper mDatabase;
    private ClothDataList clothDataList;

    public IronFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_iron, container, false);

        Helper.CUSTOMER_NAME = "";
        IRON_USER_ID = "";
        ((MainActivity) getActivity()).setActionBarTitle(Helper.INDIVIDUAL_SERVICE_NAME,R.drawable.ic_arrow_back);
        SharedPreferences sp = getActivity().getSharedPreferences("Driver", Context.MODE_PRIVATE);
        driver_id = sp.getString("driver_id", "").trim();

        rlMain = (RelativeLayout) view.findViewById(R.id.rlMain);
        editTextSearch = (EditText) view.findViewById(R.id.editTextSearch);
        ivSearch = (ImageView) view.findViewById(R.id.ivSearch);
        ivAdd = (ImageView) view.findViewById(R.id.ivAdd);

        rvCustomerList = (RecyclerView) view.findViewById(R.id.rvCustomerList);
        customerLayoutManager = new LinearLayoutManager(getActivity());
        customerLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCustomerList.setLayoutManager(customerLayoutManager);
        rvCustomerList.setHasFixedSize(true);

        getCustomerList();

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.requestFocus();
                String text = editTextSearch.getText().toString().trim();
                if (isEmpty(text)) {
                    Helper.showToast(getActivity(), "Please enter name");
                } else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rlMain.getWindowToken(), 0);
                }
            }
        });


        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });

        return view;
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<String> filterNames = new ArrayList<>();

        if (names.size() > 0) {
            //looping through existing elements
            for (String s : names) {
                //if the existing elements contains the search input
                if (s.toLowerCase().contains(text.toLowerCase())) {
                    //adding the element to filtered list
                    filterNames.add(s);
                }
            }

            //calling a method of the adapter class and passing the filtered list
            selectCustomerAdapter.filterList(filterNames);
        }
    }

    private String email2;
    String EmailPattern;

    private AppCompatEditText name, email, mobile_no, address;

    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewGroup
        ViewGroup viewGroup = getActivity().findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.custom_dialog, viewGroup, false);
        Button add = dialogView.findViewById(R.id.btn_add);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);
        name = dialogView.findViewById(R.id.name);
        email = dialogView.findViewById(R.id.email);
        mobile_no = dialogView.findViewById(R.id.mobile_no);
        address = dialogView.findViewById(R.id.address);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        email2 = email.getText().toString().trim();
        //finally creating the alert dialog and displaying it
        //emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\\\.+[a-z]+";

        final AlertDialog alertDialog = builder.create();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isEmpty(name.getText().toString())) {
                    name.setError("Enter name");
                } else if (isEmpty(address.getText().toString())) {
                    address.setError("Enter address");
                } else if (!isEmpty(mobile_no.getText().toString().trim()) && mobile_no.getText().toString().length() < 6 || mobile_no.getText().toString().length() > 13) {
                    mobile_no.setError("Enter valid phone no.");
                } else {
                    addIronUser();
                    alertDialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void addIronUser() {

        names = new ArrayList<>();
        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("Add Iron LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            getCustomerList();
                        } else {
                            Helper.showToast(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {

                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("name", name.getText().toString());
            params.put("email", email.getText().toString());
            params.put("mobile_no", mobile_no.getText().toString());
            params.put("address", address.getText().toString());
            params.put("driver_id", driver_id);
            params.put("service_type", Helper.INDIVIDUAL_SERVICE_NAME);
            API api = new API(getActivity(), apiResponse);

            api.execute(1, Services.ADD_ACTIVE_IRON_CUSTOMER_LIST, params, true);

        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    private void getCustomerList() {

        names = new ArrayList<>();
        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    response = Html.fromHtml(response).toString();
                    response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                    Helper.showLog("IRON USER LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            customerArrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                //   String book_id = result.getString("book_id");
                                String iron_user_id = result.getString("iron_user_id");
                                String user_name = result.getString("name");
                                String customer_id = result.getString("customer_id");
                                String service_type = result.getString("service_type");
                                customerDataList = new CustomerDataList();
                                //  customerDataList.setBook_id(book_id);
                                customerDataList.setCustomer_id(iron_user_id);
                                customerDataList.setCustomer_name(customer_id + "." + user_name);

                                if (service_type.equals("") || service_type.equals(Helper.INDIVIDUAL_SERVICE_NAME)){
                                    customerArrayList.add(customerDataList);
                                    names.add(customer_id + "." + user_name);
                                }
                            }

                            if (customerArrayList.size() > 0 && names.size() > 0) {
                                selectCustomerAdapter = new SelectIronUserAdapter(getActivity(), names, customerArrayList, 0);

                                rvCustomerList.setAdapter(selectCustomerAdapter);

                                int resId = R.anim.layout_animation_from_bottom;
                                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
                                rvCustomerList.setLayoutAnimation(animation);
                            }

                        } else {
                            Helper.showToast(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }

            };

            HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", driver_id);
            params.put("service_type", Helper.INDIVIDUAL_SERVICE_NAME);
            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_ACTIVE_IRON_CUSTOMER_LIST, params, true);

        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    interface OnFragmentInteractionListener {

    }

}
