package com.arccus.washtagdriver.utils;

/**
 * Created by Kaprat on 14-10-2018.
 */

public class Services {

   // public static final String BASE_URL = "http://192.168.1.106/pick/api/";
   // public static final String BASE_URL = "http://pickupmylaundry.in/api/";
    public static final String BASE_URL = "http://kaprat.com/dev/washtag/api/";
    public static final String LOGIN = BASE_URL + "driver_login";
    public static final String GET_ORDER = BASE_URL + "get_order";
    public static final String GET_ORDER_DETAIL = BASE_URL + "get_order_detail";
   // public static final String ADD_LOCATION = BASE_URL + "add_address";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotpassword";

    public static final String GET_CLOTH = BASE_URL + "sub_category_garment_name";
    public static final String GET_ACTIVE_CUSTOMER_LIST = BASE_URL  + "active_customer_list";


    public static final String SET_ORDER = BASE_URL + "Clothes_left";
    public static final String SET_DELIVERY = BASE_URL + "order_delivered";

    public static final String NO_NETWORK = "No Internet Connection";

    public static final String GET_ORDER_IRON = BASE_URL + "iron_order_list";
    public static final String GET_ORDER_DETAIL_IRON  = BASE_URL + "get_iron_order_detail";
    public static final String GET_ACTIVE_IRON_CUSTOMER_LIST = BASE_URL  + "get_iron_user";
    public static final String ADD_ACTIVE_IRON_CUSTOMER_LIST = BASE_URL  + "iron_registration";
    public static final String SET_ORDER_IRON = BASE_URL  + "add_iron_order";
    public static final String SET_DELIVERY_IRON = BASE_URL + "iron_order_delivered";
   // public static final String EDIT_PROFILE = BASE_URL + "edit_profile";
}
