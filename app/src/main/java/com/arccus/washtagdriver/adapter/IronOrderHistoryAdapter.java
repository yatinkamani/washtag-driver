package com.arccus.washtagdriver.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.model.OrderHistoryData;

import java.util.ArrayList;

/**
 * Created by Kaprat on 31-10-2018.
 */

public class IronOrderHistoryAdapter extends RecyclerView.Adapter<IronOrderHistoryAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<OrderHistoryData> orderHistoryArrayList = new ArrayList<>();
    private customButtonListener customListener;

    public IronOrderHistoryAdapter(Context context, ArrayList<OrderHistoryData> orderHistoryArrayList){
        this.context = context;
        this.orderHistoryArrayList = orderHistoryArrayList;
    }

    public interface customButtonListener {
        public void onButtonClickListener(int position);
    }

    public void setCustomButtonListener(customButtonListener listener) {
        this.customListener = listener;
    }

    @NonNull
    @Override
    public IronOrderHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_row,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final IronOrderHistoryAdapter.MyViewHolder holder, int position) {

        OrderHistoryData orderHistoryData = orderHistoryArrayList.get(position);

        final String orderNO = orderHistoryData.getOrder_no();
        holder.tvOrderNo.setText(orderNO);
       // SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        holder.tvOrderDate.setText(orderHistoryData.getOrder_date());
        holder.tvOrderDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customListener != null) {
                    customListener.onButtonClickListener(holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderHistoryArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOrderNo, tvOrderDate, tvOrderDetail;
        public MyViewHolder(View itemView) {
            super(itemView);

            tvOrderNo = (TextView) itemView.findViewById(R.id.tvOrderNo);
            tvOrderDate = (TextView) itemView.findViewById(R.id.tvOrderDate);
            tvOrderDetail = (TextView) itemView.findViewById(R.id.tvOrderDetail);
        }
    }
}
