package com.arccus.washtagdriver.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.helper.ClothItemClickListener;
import com.arccus.washtagdriver.model.ClothDataList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import static com.arccus.washtagdriver.utils.Helper.TOTAL_CLOTH_COUNT;

/**
 * Created by KapRat on 30-10-2018.
 */

public class ClothAdapter extends RecyclerView.Adapter<ClothAdapter.MyViewHolder> {

    /*  public static int sCorner = 50;
        public static int sMargin = 2;
        public static int sBorder = 7;
        public static String sColor = "#008afa";*/

    private ArrayList<ClothDataList> clothDataArrayList = new ArrayList<>();
    private ClothItemClickListener clothItemClickListener;
    private static int lastPosition = -1;
    private Context context;

    public ClothAdapter(ArrayList<ClothDataList> clothDataArrayList, ClothItemClickListener clothItemClickListener, Context context) {

        this.clothDataArrayList = clothDataArrayList;
        this.clothItemClickListener = clothItemClickListener;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.get_clothes_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final ClothDataList clothDataList = clothDataArrayList.get(position);

        /* Glide.with(context)
                .load(clothDataList.getClothInt())
                .into(holder.ivCloth);*/

        if (clothDataList.getClothBitmap() != null) {

            Glide.with(context).asBitmap()
                    .load(clothDataList.getClothBitmap())
                    .into(holder.ivCloth);
        } else {
            Glide.with(context)
                    .asBitmap()
                    .load(clothDataList.getClothUri())
                    .thumbnail(0.01f)
                    .into(new BitmapImageViewTarget(holder.ivCloth) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            holder.ivCloth.setImageBitmap(resource);
                        }
                    });
        }

        /* Glide.with(context)
                .load(clothDataList.getClothBitmap())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivCloth);*/
        /* Glide.with(context).load(clothDataList.getClothBitmap())
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(context, sCorner, sMargin, sColor, sBorder)))
                .into(holder.ivCloth);*/

        holder.tvClothName.setText(clothDataList.getClothName());
//        holder.tvNumberOfCloth.setText(clothDataList.getNumber_of_Cloth());
        holder.edClothNum.setText(clothDataList.getNumber_of_Cloth());
        holder.ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String no_of_cloth = clothDataList.getNumber_of_Cloth();
                int no_of_cloth_count = Integer.parseInt(no_of_cloth);
                clothDataList.setNumber_of_Cloth(String.valueOf(no_of_cloth_count + 1));
//                holder.tvNumberOfCloth.setText(clothDataList.getNumber_of_Cloth());
                holder.edClothNum.setText(clothDataList.getNumber_of_Cloth());
//                TOTAL_CLOTH_COUNT = TOTAL_CLOTH_COUNT + 1;
//                clothItemClickListener.onClothItemClick(clothDataArrayList);
            }
        });

        holder.ivMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String no_of_cloth = clothDataList.getNumber_of_Cloth();
                int no_of_cloth_count = Integer.parseInt(no_of_cloth);
                if (no_of_cloth_count != 0) {
                    clothDataList.setNumber_of_Cloth(String.valueOf(no_of_cloth_count - 1));
//                    holder.tvNumberOfCloth.setText(clothDataList.getNumber_of_Cloth());
                    holder.edClothNum.setText(clothDataList.getNumber_of_Cloth());
//                    TOTAL_CLOTH_COUNT = TOTAL_CLOTH_COUNT - 1;
//                    clothItemClickListener.onClothItemClick(clothDataArrayList);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return clothDataArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivCloth, ivPlus, ivMinus;
        private TextView tvClothName;
        private EditText edClothNum;

        MyViewHolder(View itemView) {
            super(itemView);

            ivCloth = (ImageView) itemView.findViewById(R.id.ivCloth);
            ivPlus = (ImageView) itemView.findViewById(R.id.ivPlus);
            ivMinus = (ImageView) itemView.findViewById(R.id.ivMinus);
            tvClothName = (TextView) itemView.findViewById(R.id.tvClothName);
            edClothNum = (EditText) itemView.findViewById(R.id.tvNumerOfCloth);
//            tvNumerOfCloth = (TextView) itemView.findViewById(R.id.tvNumerOfCloth);

            edClothNum.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (edClothNum.getText().toString().equals("")){
                        edClothNum.setText("0");
                    }

                    if (edClothNum.getText().toString().length() > 1 && edClothNum.getText().toString().startsWith("0") ){
                        Log.e("Tag Size", ""+edClothNum.getText().toString().substring(1));
                        edClothNum.setText(edClothNum.getText().toString().substring(1));
                        edClothNum.setSelection(edClothNum.getText().length());
                    }

                    if (Integer.parseInt(edClothNum.getText().toString()) < 0){
                        edClothNum.setText("0");
                    }

                    clothDataArrayList.get(getAdapterPosition()).setNumber_of_Cloth(edClothNum.getText().toString());
                    clothItemClickListener.onclothItemClick(clothDataArrayList);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        }
    }

}
