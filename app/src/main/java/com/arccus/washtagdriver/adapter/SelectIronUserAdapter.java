package com.arccus.washtagdriver.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.activity.ClothTabbedActivity;
import com.arccus.washtagdriver.activity.IronOrderHistoryActivity;
import com.arccus.washtagdriver.model.CustomerDataList;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.util.ArrayList;

import static com.arccus.washtagdriver.utils.Helper.IRON_USER_ID;
import static com.arccus.washtagdriver.utils.Helper.CUSTOMER_NAME;

/*
 * Created by KCS on 23-Nov-18.
 * Email : info.kaprat@gmail.com
 */

public class SelectIronUserAdapter extends RecyclerView.Adapter<SelectIronUserAdapter.MyViewHolder> implements FastScrollRecyclerView.SectionedAdapter{

    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<CustomerDataList> customerArrayList = new ArrayList<>();
    private Context context;
    private int fromWhere;

    public SelectIronUserAdapter(Context context, ArrayList<String> names, ArrayList<CustomerDataList> customerArrayList, int fromWhere){

        this.names = names;
        this.context = context;
        this.fromWhere = fromWhere;
        this.customerArrayList = customerArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_list_row,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final CustomerDataList customerDataList = customerArrayList.get(position);

        holder.tv_customer_name.setText(names.get(position));
        Log.e("name"," " +names.get(position));
        holder.tv_customer_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CUSTOMER_NAME = names.get(position);
                for (int i = 0; i< customerArrayList.size(); i++){
                    CustomerDataList customerDataList1 = customerArrayList.get(i);
                    String customer_name = customerDataList1.getCustomer_name();
                    if(customer_name.equalsIgnoreCase(CUSTOMER_NAME)){
                        IRON_USER_ID = customerDataList1.getCustomer_id();
                        Log.e("IRON_USER_ID","  "+IRON_USER_ID);
                        break;
                    }
                }

                if(fromWhere == 0) {
                    Intent intent = new Intent(context, ClothTabbedActivity.class);
                    context.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(context, IronOrderHistoryActivity.class);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return names.size();
    }


    public void filterList(ArrayList<String> filterdNames) {
        this.names = filterdNames;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        return names.get(position).substring(0,1);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_customer_name;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_customer_name = (TextView) itemView.findViewById(R.id.tv_customer_name);
        }
    }
}
