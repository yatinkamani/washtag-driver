package com.arccus.washtagdriver.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.washtagdriver.R;
import com.arccus.washtagdriver.model.OrderDataList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

/**
 * Created by Kaprat on 31-10-2018.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {

    private ArrayList<OrderDataList> orderDataArrayList = new ArrayList<>();
    private Context context;

    public  OrderListAdapter(Context context, ArrayList<OrderDataList> orderDataArrayList){
        this.context = context;
        this.orderDataArrayList = orderDataArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_row,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        OrderDataList orderDataList = orderDataArrayList.get(position);

       /* Glide.with(context)
                .load(orderDataList.getOrderInt())
                .into(holder.ivCloth);*/

       if (orderDataList.getOrder_image() != null){

           Glide.with(context)
                   .load(orderDataList.getOrder_image())
                   .into(holder.ivCloth);
       }else {
           Glide.with(context).asBitmap()
                   .load(orderDataList.getOrder_url())
                   .thumbnail(0.01f)
                   .into(new BitmapImageViewTarget(holder.ivCloth){
                       @Override
                       protected void setResource(Bitmap resource) {
                           holder.ivCloth.setImageBitmap(resource);
                       }
                   });
       }

        holder.tvClothName.setText(orderDataList.getOrder_name());
        holder.tvNumerOfCloth.setText(orderDataList.getOrder_quantity());

    }

    @Override
    public int getItemCount() {
        return orderDataArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivCloth;
        private TextView tvClothName, tvNumerOfCloth;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivCloth = (ImageView)itemView.findViewById(R.id.ivCloth);
            tvClothName = (TextView)itemView.findViewById(R.id.tvClothName);
            tvNumerOfCloth = (TextView)itemView.findViewById(R.id.tvNumerOfCloth);
        }
    }
}
