package com.arccus.washtagdriver.model;

/*
 * Created by KCS on 22-Nov-18.
 * Email : info.kaprat@gmail.com
 */
public class CustomerDataList {

    private String customer_name;

    private String book_id;

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    private String customer_id;
}
