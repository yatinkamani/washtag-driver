package com.arccus.washtagdriver.model;

import java.io.Serializable;

/*
 * Created by KCS on 21-Nov-18.
 * Email : info.kaprat@gmail.com
 */
public class SelectedOrderList implements Serializable{

    private String clothCategory;
    private String clothName;

    public String getCloth_id() {
        return cloth_id;
    }

    public void setCloth_id(String cloth_id) {
        this.cloth_id = cloth_id;
    }

    private String cloth_id;

    public String getNumber_of_Cloth() {
        return number_of_Cloth;
    }

    public void setNumber_of_Cloth(String number_of_Cloth) {
        this.number_of_Cloth = number_of_Cloth;
    }

    private String number_of_Cloth;

    public String getClothCategory() {
        return clothCategory;
    }

    public void setClothCategory(String clothCategory) {
        this.clothCategory = clothCategory;
    }

    public String getClothName() {
        return clothName;
    }

    public void setClothName(String clothName) {
        this.clothName = clothName;
    }

    public String getClothUri() {
        return clothUri;
    }

    public void setClothUri(String clothUri) {
        this.clothUri = clothUri;
    }

    private String clothUri;

    public String getTotalCloth() {
        return totalCloth;
    }

    public void setTotalCloth(String totalCloth) {
        this.totalCloth = totalCloth;
    }

    private String totalCloth;
}
