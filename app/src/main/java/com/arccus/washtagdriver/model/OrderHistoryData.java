package com.arccus.washtagdriver.model;

/**
 * Created by Kaprat on 31-10-2018.
 */

public class OrderHistoryData {

    private String order_no;

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    private String order_date;
}
