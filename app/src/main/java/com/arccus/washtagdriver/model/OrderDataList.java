package com.arccus.washtagdriver.model;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by Kaprat on 31-10-2018.
 */

public class OrderDataList implements Serializable{

   /* public int getOrderInt() {
        return orderInt;
    }

    public void setOrderInt(int orderInt) {
        this.orderInt = orderInt;
    }

    private int orderInt;*/

    private String order_id;
    private String order_name;
    private String order_quantity;
    private String order_url;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getOrder_quantity() {
        return order_quantity;
    }

    public void setOrder_quantity(String order_quantity) {
        this.order_quantity = order_quantity;
    }

    public String getOrder_category() {
        return order_category;
    }

    public void setOrder_category(String order_category) {
        this.order_category = order_category;
    }

    private String order_category;

    public Bitmap getOrder_image() {
        return order_image;
    }

    public void setOrder_image(Bitmap order_image) {
        this.order_image = order_image;
    }

    public String getOrder_url() {
        return order_url;
    }

    public void setOrder_url(String order_url) {
        this.order_url = order_url;
    }

    private Bitmap order_image;
}
