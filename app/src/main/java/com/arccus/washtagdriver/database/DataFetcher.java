package com.arccus.washtagdriver.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;

import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.utils.Helper;

import java.util.ArrayList;

import static com.arccus.washtagdriver.database.DatabaseHelper.COLUMN_CLOTH_CATEGORY;
import static com.arccus.washtagdriver.database.DatabaseHelper.COLUMN_ID;
import static com.arccus.washtagdriver.database.DatabaseHelper.COLUMN_IMAGE;
import static com.arccus.washtagdriver.database.DatabaseHelper.COLUMN_NAME;
import static com.arccus.washtagdriver.database.DatabaseHelper.COLUMN_NO_OF_CLOTH;
import static com.arccus.washtagdriver.database.DatabaseHelper.TABLE_NAME;

/**
 * Created by Kaprat on 02-11-2018.
 */

public class DataFetcher extends Thread{
    private final DataFetchListner mListener;
    private final SQLiteDatabase mDb;
    private String cloth_category;

    public DataFetcher(DataFetchListner listener, SQLiteDatabase db, String cloth_category) {
        mListener = listener;
        mDb = db;
        this.cloth_category = cloth_category;
    }

    @Override
    public void run() {
        //Select all data
        Cursor cursor = mDb.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_CLOTH_CATEGORY +" = " + cloth_category, null);
        // checking database is not empty
        if (cursor.getCount() > 0) {

            ArrayList<ClothDataList> clothDataArrayList = new ArrayList<>();
            // looping through all values and adding to list
            if (cursor.moveToFirst()) {
                do {
                    ClothDataList data = new ClothDataList();

                    data.setCloth_id(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                    data.setClothName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
                    data.setClothCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CLOTH_CATEGORY)));
                    data.setNumber_of_Cloth(cursor.getString(cursor.getColumnIndex(COLUMN_NO_OF_CLOTH)));
                    data.setClothBitmap(Helper.getBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(COLUMN_IMAGE))));

                    // data.setName(cursor.getString(cursor.getColumnIndex(TITLE)));
                    // adding data

                    clothDataArrayList.add(data);
                } while (cursor.moveToNext());
            }

            publishFlower(clothDataArrayList);
        }
    }
    // used to pass all data
    public void publishFlower(final ArrayList<ClothDataList> clothDataArrayList) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                // passing data through onDeliverData()
                mListener.onDeliverData(clothDataArrayList);
            }
        });
    }
}
