package com.arccus.washtagdriver.database;

import com.arccus.washtagdriver.model.ClothDataList;

import java.util.ArrayList;

/**
 * Created by Kaprat on 02-11-2018.
 */

public interface DataFetchListner {

    void onDeliverData(ArrayList<ClothDataList> stickerArrayLists);
    void onHideDialog();
}
