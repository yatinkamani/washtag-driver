package com.arccus.washtagdriver.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import com.arccus.washtagdriver.model.ClothDataList;
import com.arccus.washtagdriver.utils.Helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaprat on 02-11-2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "ProductInfo";
    // Database Version
    private static final int DB_VERSION = 1;
    // Table name
    public static final String TABLE_NAME = "ClothInfo";
    // Drop table query
    public static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    // Select all query
    public static final String GET_IMAGE_QUERY = "SELECT * FROM " + TABLE_NAME;
    // image table column names
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "cloth_name";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_NO_OF_CLOTH = "no_of_cloth";
    public static final String COLUMN_CLOTH_CATEGORY = "cloth_category";

    public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME +
            "(" + COLUMN_ID + " TEXT PRIMARY KEY not null," +
            COLUMN_NAME + " TEXT not null, " +
            COLUMN_CLOTH_CATEGORY + " TEXT not null, " +
            COLUMN_NO_OF_CLOTH + " TEXT not null, "+
            COLUMN_IMAGE + " blob not null)";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("DB CREATED","False");
        db.execSQL(CREATE_TABLE_QUERY);
        Log.e("DB CREATED","TRUE");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL(DROP_QUERY);
        // Create tables again
        this.onCreate(db);
    }


    public Bitmap getBitmap(String bid){

        Bitmap bitmap = null;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT image FROM " + TABLE_NAME + " WHERE " + COLUMN_ID +" = " + bid, null);
        if (cursor.moveToFirst()) {
            bitmap = Helper.getBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(COLUMN_IMAGE)));
        }
        return bitmap;
    }

    public void addData(ClothDataList dataModel){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // Log.e("PATH",""+dataModel.getSticker_id());
        values.put(COLUMN_ID, dataModel.getCloth_id());
        values.put(COLUMN_NAME, dataModel.getClothName());
        values.put(COLUMN_CLOTH_CATEGORY, dataModel.getClothCategory());
        values.put(COLUMN_NO_OF_CLOTH, dataModel.getNumber_of_Cloth());
        values.put(COLUMN_IMAGE, Helper.getPictureByteOfArray(dataModel.getClothBitmap()));
        // Inserting row
        db.insertWithOnConflict(TABLE_NAME,null,values, SQLiteDatabase.CONFLICT_REPLACE);
        // Closing DataBase connection

        // Log.e("DATA ADDED","TRUE");
        db.close();
    }

    public void deleteDatabase(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME);
        db.close();
    }

    public void fetchData(DataFetchListner listener, String cloth_category) {
    //  DataFetcher method accepts listener and writable database(step 6)
        DataFetcher fetcher = new DataFetcher(listener, this.getWritableDatabase(), cloth_category);
        fetcher.start();
    }

    public boolean updateData(ClothDataList dataModel)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // Log.e("PATH",""+dataModel.getSticker_id());
        values.put(COLUMN_ID, dataModel.getCloth_id());
        values.put(COLUMN_NAME, dataModel.getClothName());
        values.put(COLUMN_CLOTH_CATEGORY, dataModel.getClothCategory());
        values.put(COLUMN_NO_OF_CLOTH, dataModel.getNumber_of_Cloth());
        values.put(COLUMN_IMAGE, Helper.getPictureByteOfArray(dataModel.getClothBitmap()));
        return db.update(TABLE_NAME, values, COLUMN_ID + "=" + dataModel.getCloth_id(), null)>0;
    }

    public ArrayList<ClothDataList> getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME ,null);
        // checking database is not empty
        ArrayList<ClothDataList> clothDataArrayList = new ArrayList<>();
        if (cursor.getCount() > 0) {

            // looping through all values and adding to list
            if (cursor.moveToFirst()) {
                do {
                    ClothDataList data = new ClothDataList();

                    data.setCloth_id(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                    data.setClothName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
                    data.setClothCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CLOTH_CATEGORY)));
                    data.setNumber_of_Cloth(cursor.getString(cursor.getColumnIndex(COLUMN_NO_OF_CLOTH)));
                    data.setClothBitmap(Helper.getBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(COLUMN_IMAGE))));

                    // data.setName(cursor.getString(cursor.getColumnIndex(TITLE)));
                    // adding data

                    clothDataArrayList.add(data);
                } while (cursor.moveToNext());
            }
        }
        return clothDataArrayList;
    }
}
