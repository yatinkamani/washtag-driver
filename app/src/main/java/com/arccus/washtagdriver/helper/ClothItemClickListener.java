package com.arccus.washtagdriver.helper;

import com.arccus.washtagdriver.model.ClothDataList;

import java.util.ArrayList;

/**
 * Created by Kaprat on 30-10-2018.
 */

public interface ClothItemClickListener {

    void onclothItemClick(ArrayList<ClothDataList> clothDataArrayList);
}
